"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var PouchDB = require("pouchdb/dist/pouchdb");
var PouchFind = require("pouchdb-find/lib/index");
PouchDB.plugin(PouchFind);
var refundOrderService = (function () {
    function refundOrderService(_router) {
        this._router = _router;
        this.ordersDB = new PouchDB('orders');
        this.refundOrdersDB = new PouchDB('refundorders');
    }
    refundOrderService.prototype.getRefundOrder = function (orderId) {
        var self = this;
        return self.ordersDB.find({
            selector: { orderId: parseInt(orderId) }
        });
    };
    refundOrderService.prototype.saveRefundOrder = function (refundOrderDoc) {
        var self = this;
        console.log("refundOrderDoc: ", refundOrderDoc);
        return self.refundOrdersDB.put(refundOrderDoc);
    };
    refundOrderService.prototype.updateRefundOrder = function (refundOrderDoc) {
        var self = this;
        console.log("refundOrderDoc from update: ", refundOrderDoc);
        return self.refundOrdersDB.put(refundOrderDoc);
    };
    refundOrderService.prototype.checkRefunded = function (orderNumber) {
        console.log("orderNumber", orderNumber);
        var self = this;
        return self.refundOrdersDB.find({
            selector: { refundOrderKey: { $eq: orderNumber } }
        });
    };
    refundOrderService.prototype.getAllRefunds = function () {
        var self = this;
        return self.refundOrdersDB.allDocs({
            include_docs: true,
            attachments: true,
            descending: true
        });
    };
    refundOrderService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router])
    ], refundOrderService);
    return refundOrderService;
}());
exports.refundOrderService = refundOrderService;
//# sourceMappingURL=refundorder.service.js.map