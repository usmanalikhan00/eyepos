import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
import { User } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib/index'
// PouchDB.plugin(PouchFind)
import * as moment from "moment";

// var users = [
//   new User('1','aajeel','admin@aajeel.com','adm9', 'admin'),
//   new User('2','khan','operator@aajeel.com','ope9', 'operator')
// ];

@Injectable()
export class AuthenticationService {
  public usersDB = new PouchDB('users');
  
  authenticatedUser: any = [];
  
  constructor(private _router: Router){
    
  }

  // logout() {
  //   localStorage.removeItem("user");
  //   this._router.navigate(['login']);
  // }
  logout() {
    // var test = localStorage.getItem("user");
    //     console.log('local storage Item', test);
    localStorage.removeItem("user");
    localStorage.removeItem("userRole");
    this._router.navigate(['login']);
  }
  login(email, password){
    var self = this;
    // var authenticatedUser = self.users.find(u => u.email === user.email);
    // if (authenticatedUser && authenticatedUser.password === user.password){
    //   localStorage.setItem("user", authenticatedUser.email);
    //   return true;
    // }
    // console.log(user);
    // return false;
    return self.usersDB.find({
        selector: {email:email,
                  password:password
                }
    });

    // localStorage.setItem("user", authenticatedUser.email);
  }

   loggedIn(){
    if (localStorage.getItem("user") === null){
      return false
    }
    return true
  }
}
