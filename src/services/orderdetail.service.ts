import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
import { Product } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib/index'
PouchDB.plugin(PouchFind)

@Injectable()
export class orderDetailService {

	public ordersDB = new PouchDB('orders');
	constructor(){}

	findOrder(orderid){
		var self = this;
		return self.ordersDB.find({
        	selector: {orderId:orderid}
		});
	}

}