"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var PouchDB = require("pouchdb/dist/pouchdb");
var PouchFind = require("pouchdb-find/lib/index");
PouchDB.plugin(PouchFind);
var ordersService = (function () {
    function ordersService(_router) {
        this._router = _router;
        this.ordersDB = new PouchDB('orders');
        this.allOrders = [];
    }
    ordersService.prototype.getAllOrders = function () {
        var self = this;
        return self.ordersDB.allDocs({
            include_docs: true,
            attachments: true,
            'endkey': '_design'
        });
    };
    ordersService.prototype.getResultsByMonth = function (selectedMonth) {
        var self = this;
        return self.ordersDB.find({
            selector: { orderMonth: selectedMonth }
        });
    };
    ordersService.prototype.getTodaySales = function (orderDay, orderMonth) {
        var self = this;
        return self.ordersDB.find({
            selector: { orderDay: { $eq: orderDay },
                orderMonth: { $eq: orderMonth } }
        });
    };
    ordersService.prototype.filterOrders = function (minDate, maxDate) {
        var self = this;
        console.log(minDate, maxDate);
        return self.ordersDB.createIndex({
            index: {
                fields: ['_id']
            }
        }).then(function () {
            return self.ordersDB.find({
                selector: {
                    $and: [
                        {
                            _id: {
                                $exists: true
                            }
                        },
                        {
                            _id: {
                                $lte: maxDate
                            }
                        },
                        {
                            _id: {
                                $gte: minDate
                            }
                        }
                    ]
                },
                sort: [{ _id: 'desc' }]
            });
        });
    };
    ordersService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router])
    ], ordersService);
    return ordersService;
}());
exports.ordersService = ordersService;
//# sourceMappingURL=orders.service.js.map