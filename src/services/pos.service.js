"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var PouchDB = require("pouchdb/dist/pouchdb");
var PouchFind = require("pouchdb-find/lib/index");
PouchDB.plugin(PouchFind);
var PosService = (function () {
    function PosService() {
        this.productsDB = new PouchDB('products');
        this.ordersDB = new PouchDB('orders');
        this.customerDB = new PouchDB('customers');
    }
    PosService.prototype.getAllProducts = function () {
        var self = this;
        self.allProducts = [];
        return self.productsDB.allDocs({
            include_docs: true,
            attachments: true,
            descending: true
        });
    };
    PosService.prototype.updateStockValue = function (stockArrayToEdit) {
        var self = this;
        console.log("itemsArrayToPush: ", stockArrayToEdit);
        return self.productsDB.bulkDocs(stockArrayToEdit);
    };
    PosService.prototype.getAllOrders = function () {
        var self = this;
        return self.ordersDB.allDocs({
            include_docs: true,
            attachments: true,
            descending: true
        });
    };
    PosService.prototype.searchProduct = function (barCode) {
        var self = this;
        return self.productsDB.find({
            selector: { productBarCode: barCode }
        });
    };
    PosService.prototype.saveOrderDoc = function (orderDoc) {
        var self = this;
        self.ordersDB.put(orderDoc);
    };
    PosService.prototype.getAllCustomers = function () {
        var self = this;
        return self.customerDB.allDocs({
            include_docs: true,
            attachments: true,
            descending: true
        });
    };
    PosService.prototype.addCustomer = function (customerDetails) {
        var self = this;
        console.log("Customer Details From Service: ", customerDetails);
        return self.customerDB.put(customerDetails);
    };
    PosService.prototype.getCustomerPage = function (limit, skip) {
        var self = this;
        return self.customerDB.allDocs({
            include_docs: true,
            attachments: true,
            descending: true,
            skip: skip,
            limit: limit
        });
    };
    PosService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [])
    ], PosService);
    return PosService;
}());
exports.PosService = PosService;
//# sourceMappingURL=pos.service.js.map