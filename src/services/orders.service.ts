import { Injectable }  from '@angular/core';
import * as moment from "moment";
import { Router } from  '@angular/router';
import { Product } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib/index'
PouchDB.plugin(PouchFind)

@Injectable()
export class ordersService {
  
  public ordersDB = new PouchDB('orders');

  constructor(private _router: Router){}
  allOrders = [];

  getAllOrders(){
    var self = this; 
    return self.ordersDB.allDocs({
      include_docs: true,
      attachments: true,
      'endkey':'_design'
    });
  }

  getFirstPage(options){
    var self = this;
    console.log("LIMIT FOR THE PAGE IS:---", options.limit) 
    return self.ordersDB.allDocs({
      include_docs: true,
      limit: options.limit,
      startkey:options.startkey,
      attachments: true,
      'endkey':'_design'
    });
  }

  getNextPage(options){
    var self = this; 
    return self.ordersDB.allDocs({
      include_docs: true,
      startkey: options.startKey,
      limit: options.limit,
      skip: options.skip,
      attachments: true,
      'endkey':'_design'
    });
  }




  getResultsByMonth(selectedMonth){
    var self = this;
    return self.ordersDB.find({
      selector: {orderMonth:selectedMonth}
    });
  }

  removeOrder(order){
    var self = this;
    return self.ordersDB.remove(order);
  }

  getResultsByYear(selectedYear){
    var self = this;

    return self.ordersDB.find({
      selector: {orderYear:selectedYear}
    });
  }
  getResultsByMonthYear(selectedYear, selectedMonth){
    var self = this;

    return self.ordersDB.find({
      selector: {
        // $and:{orderYear:selectedYear,orderMonth:selectedMonth}
        $and: [
            {
              orderYear:{ 
                $eq: selectedYear
              }
            },
            {
              orderMonth:{
                $eq:selectedMonth
              }
            }
          ]
      }
    });
  }


  getTodaySales(orderDay, orderMonth, orderYear){
    var self = this;
    return self.ordersDB.createIndex({
      index: {
        fields: ['orderDay', 'orderMonth', 'orderYear']
      }
    }).then(function(){
      return self.ordersDB.find({
        selector: {orderDay:{$eq: orderDay},
                   orderMonth: {$eq: orderMonth},
                   orderYear: {$eq: orderYear}}
      });    
    });
  }

  filterOrders(minDate, maxDate){
    var self = this
        console.log(minDate, maxDate)
    return self.ordersDB.createIndex({
      index: {
        fields: ['_id']
      }
    }).then(function(){
      return self.ordersDB.find({
        selector: { 
          $and: [
            {
              _id:{
                $exists:true
              }
            },
            {
              _id:{
                $lte: maxDate
              }
            },
            {
              _id:{
                $gte: minDate
              }
            }

          ]
        },
        sort: [{_id:'desc'}]
      })
    })
  }
}