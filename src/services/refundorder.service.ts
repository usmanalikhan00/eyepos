import { Injectable }  from '@angular/core';
import * as moment from "moment";
import { Router } from  '@angular/router';
import { Product } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib/index'
PouchDB.plugin(PouchFind)

@Injectable()
export class refundOrderService {
  
  public ordersDB = new PouchDB('orders');
  public refundOrdersDB = new PouchDB('refundorders'); 

  constructor(private _router: Router){}

  getRefundOrder(orderId){
    var self = this;
    	// console.log(orderId);
    return self.ordersDB.find({
      selector: {orderId:parseInt(orderId)}
    });
  }
  saveRefundOrder(refundOrderDoc){
  	var self = this;
  	console.log("refundOrderDoc: ", refundOrderDoc);
  	return self.refundOrdersDB.put(refundOrderDoc);
  }  
  updateRefundOrder(refundOrderDoc){
  	var self = this;
  	console.log("refundOrderDoc from update: ", refundOrderDoc);
  	return self.refundOrdersDB.put(refundOrderDoc);
  }
  checkRefunded(orderNumber){
  	console.log("orderNumber", orderNumber);
  	var self = this;
  	return self.refundOrdersDB.find({
      selector: {refundOrderKey:{$eq: orderNumber}}
    });	
  }
  getAllRefunds(){
  	var self = this; 
    return self.refundOrdersDB.allDocs({
      include_docs: true,
      attachments: true,
      descending: true
    });
  } 
  getResultsByMonth(selectedMonth){
    var self = this;
    return self.refundOrdersDB.find({
      selector: {refundMonth:selectedMonth}
    });
  }
}