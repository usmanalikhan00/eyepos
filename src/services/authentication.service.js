"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var PouchDB = require("pouchdb/dist/pouchdb");
var AuthenticationService = (function () {
    function AuthenticationService(_router) {
        this._router = _router;
        this.usersDB = new PouchDB('users');
        this.authenticatedUser = [];
    }
    AuthenticationService.prototype.logout = function () {
        localStorage.removeItem("user");
        localStorage.removeItem("userRole");
        this._router.navigate(['login']);
    };
    AuthenticationService.prototype.login = function (email, password) {
        var self = this;
        return self.usersDB.find({
            selector: { email: email,
                password: password
            }
        });
    };
    AuthenticationService.prototype.loggedIn = function () {
        if (localStorage.getItem("user") === null) {
            return false;
        }
        return true;
    };
    AuthenticationService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [router_1.Router])
    ], AuthenticationService);
    return AuthenticationService;
}());
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=authentication.service.js.map