"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var PouchDB = require("pouchdb/dist/pouchdb");
var PouchFind = require("pouchdb-find/lib/index");
PouchDB.plugin(PouchFind);
var ProductsService = (function () {
    function ProductsService(_zone) {
        this._zone = _zone;
        this.allProducts = [];
        this.DB = new PouchDB('products');
    }
    ProductsService.prototype.saveProduct = function (product) {
        var self = this;
        var newDoc = {
            "_id": new Date().toISOString(),
            "name": product.name,
            "price": product.price,
            "stockval": product.stockval,
            "productBarCode": product.productBarCode
        };
        return self.DB.put(newDoc);
    };
    ProductsService.prototype.editProduct = function (newValues, product) {
        var self = this;
        var newDoc = {
            "_id": product._id,
            "_rev": product._rev,
            "name": newValues.name,
            "price": newValues.price,
            "stockval": newValues.stockval,
            "productBarCode": newValues.productBarCode
        };
        console.log("Product to edit:", newDoc);
        return self.DB.put(newDoc);
    };
    ProductsService.prototype.getAllProducts = function () {
        var self = this;
        self.allProducts = [];
        return self.DB.allDocs({
            include_docs: true,
            attachments: true,
            descending: true
        });
    };
    ProductsService.prototype.deleteProduct = function (doc) {
        var self = this;
        return self.DB.get(doc);
    };
    ProductsService.prototype.updateRedundStock = function (itemsToRefund) {
        var self = this;
        console.log("ID OF THE PRODUCT TO UPDATE FROM PRODUCT SERVICE:", itemsToRefund);
        return self.DB.get(itemsToRefund);
    };
    ProductsService.prototype.updateRefundStock = function (itemsToRefund) {
        var self = this;
        console.log("ITEMS TO REFUND FROOM PRODUCT SERVICE: ", itemsToRefund);
        return self.DB.put(itemsToRefund);
    };
    ProductsService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [core_1.NgZone])
    ], ProductsService);
    return ProductsService;
}());
exports.ProductsService = ProductsService;
//# sourceMappingURL=products.service.js.map