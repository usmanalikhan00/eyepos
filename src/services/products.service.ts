import { Injectable, NgZone }  from '@angular/core';
import { Router } from  '@angular/router';
import { Product } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib/index'
PouchDB.plugin(PouchFind)

@Injectable()
export class ProductsService {
  
  allProducts:any = [];
  docToDel: any;
  
  DB = new PouchDB('products');

  constructor(private _zone: NgZone){
  }

  saveProduct(product){
    var self = this;
    // self.DB.info().then(function (info) {
    //   console.log(info);
    // })
    // var newDoc = {
    //   "_id": new Date().toISOString(),
    //   "name": product.name,
    //   "price": product.price,
    //   "stockval": product.stockval,
    //   "productBarCode": product.productBarCode
    // };
    product._id = new Date().toISOString()
    console.log("Product TO ADD IN SERVICE:", product);
    return self.DB.put(product)
  }
  editProduct(newValues){
    var self = this;
    console.log("Product to edit IN SERVICE:", newValues);
    return self.DB.put(newValues);
  }
  getAllProducts(){
    var self = this; 
    self.allProducts= [];
    return self.DB.allDocs({
      include_docs: true,
      attachments: true,
      descending: true
    });
  }
  deleteProduct(doc) {
    var self = this;
    return self.DB.get(doc);
  }

  updateRedundStock(itemsToRefund){
    var self = this;
    console.log("ID OF THE PRODUCT TO UPDATE FROM PRODUCT SERVICE:", itemsToRefund);
    return self.DB.get(itemsToRefund);
  }
  updateRefundStock(itemsToRefund){
    var self = this;
    console.log("ITEMS TO REFUND FROOM PRODUCT SERVICE: ", itemsToRefund);
    return self.DB.put(itemsToRefund);
  }

  
}
