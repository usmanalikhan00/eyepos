import { Injectable }  from '@angular/core';
import { Router } from  '@angular/router';
import { Product } from '../models/model-index'
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib/index'
PouchDB.plugin(PouchFind)

@Injectable()
export class PosService {
  
  allProducts: any;
  
  public productsDB = new PouchDB('products');
  public ordersDB = new PouchDB('orders');
  public customerDB = new PouchDB('customers');
  
  constructor(){}

  getAllProducts(){
    var self = this; 
    self.allProducts= [];
    return self.productsDB.allDocs({
      include_docs: true,
      attachments: true,
      descending: true
    });
  }
  // saveOrder(product){
  //   var self = this;
  //   // self.DB.info().then(function (info) {
  //   //   console.log(info);
  //   // })
  //   var newDoc = {
  //     "_id": new Date().toISOString(),
  //     "name": product.name,
  //     "price": product.price,
  //     "stockval": product.stockval
  //   };
  //   self.ordersDB.put(newDoc, function callback(err, result) {
  //     if (!err) {
  //       console.log("Product to save:", newDoc);
  //       console.log("Product to put ID:", newDoc._id);
  //     } else {
  //         console.log(err);
  //     }
  //   });
  // }
  updateStockValue(stockArrayToEdit){
    var self = this;
    console.log("itemsArrayToPush: ", stockArrayToEdit);
    return self.productsDB.bulkDocs(stockArrayToEdit);
  }
  getAllOrders(){
    var self = this; 
    return self.ordersDB.allDocs({
      include_docs: true,
      attachments: true,
      descending: true,
      limit: 5,
      skip: 2
    });
  }
  searchProduct(barCode){
    var self = this;
    return self.productsDB.find({
        selector: {productBarCode:barCode}
    });
  }
  saveOrderDoc(orderDoc){
    var self = this;
    self.ordersDB.put(orderDoc);
  }
  getAllCustomers(){
    var self = this; 
    return self.customerDB.allDocs({
      include_docs: true,
      attachments: true,
      descending: true
    });
  }
  addCustomer(customerDetails){
    var self = this;
    console.log("Customer Details From Service: ", customerDetails);
    return self.customerDB.put(customerDetails);
  }
  getCustomerPage(limit, skip){
    var self = this; 
    return self.customerDB.allDocs({
      include_docs: true,
      attachments: true,
      descending: true,
      skip: skip,
      limit: limit
    });  
  }
}