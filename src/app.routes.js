"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var components_index_1 = require("./components/components-index");
var auth_gaurd_1 = require("./_gaurds/auth.gaurd");
exports.ROUTES = [
    { path: '', component: components_index_1.Login },
    {
        path: 'home',
        component: components_index_1.Home,
        canActivate: [auth_gaurd_1.AuthGuard]
    },
    {
        path: 'invoice',
        component: components_index_1.Invoice,
        canActivate: [auth_gaurd_1.AuthGuard]
    },
    {
        path: 'about',
        component: components_index_1.About,
        canActivate: [auth_gaurd_1.AuthGuard]
    },
    {
        path: 'transactions',
        component: components_index_1.ordersComponent,
        canActivate: [auth_gaurd_1.AuthGuard]
    },
    {
        path: 'transactions/:id',
        component: components_index_1.orderDetailComponent,
        canActivate: [auth_gaurd_1.AuthGuard]
    },
    {
        path: 'pos',
        component: components_index_1.posComponent,
        canActivate: [auth_gaurd_1.AuthGuard]
    },
    {
        path: 'inventory',
        component: components_index_1.productComponent,
        canActivate: [auth_gaurd_1.AuthGuard]
    },
    { path: 'login',
        component: components_index_1.Login
    },
    { path: '**',
        component: components_index_1.NoContent
    },
];
//# sourceMappingURL=app.routes.js.map