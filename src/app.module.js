"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var app_component_1 = require("./app.component");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var app_routes_1 = require("./app.routes");
var auth_gaurd_1 = require("./_gaurds/auth.gaurd");
var authentication_service_1 = require("./services/authentication.service");
var components_index_1 = require("./components/components-index");
var angular2_datatable_1 = require("angular2-datatable");
var http_1 = require("@angular/http");
var ng2_modal_1 = require("ng2-modal");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var datepicker_1 = require("ngx-bootstrap/datepicker");
var ng2_toasty_1 = require("ng2-toasty");
var ngx_electron_1 = require("ngx-electron");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [platform_browser_1.BrowserModule,
                ng2_toasty_1.ToastyModule.forRoot(),
                router_1.RouterModule.forRoot(app_routes_1.ROUTES, { useHash: true }),
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                angular2_datatable_1.DataTableModule,
                ng2_modal_1.ModalModule,
                http_1.HttpModule,
                ngx_electron_1.NgxElectronModule,
                datepicker_1.BsDatepickerModule.forRoot(),
                ngx_bootstrap_1.AlertModule.forRoot()],
            exports: [platform_browser_1.BrowserModule,
                ngx_bootstrap_1.AlertModule,
                components_index_1.processOrder],
            declarations: [app_component_1.AppComponent,
                components_index_1.About,
                components_index_1.NoContent,
                components_index_1.processOrder,
                components_index_1.Login,
                components_index_1.Invoice,
                components_index_1.Home,
                components_index_1.productComponent,
                components_index_1.DataFilterPipe,
                components_index_1.posComponent,
                components_index_1.ordersComponent,
                components_index_1.ordersFilterPipe,
                components_index_1.orderDetailComponent],
            providers: [auth_gaurd_1.AuthGuard, authentication_service_1.AuthenticationService],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map