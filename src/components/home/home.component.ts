import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service'
import { User } from '../../models/model-index'

@Component({
  selector: 'home',
  styleUrls: ['components/home/home.template.css'],
  templateUrl: 'components/home/home.template.html',
  providers: [AuthenticationService]
})

export class Home {

    constructor(private _service: AuthenticationService,
                private _router: Router){}

     ngOnInit(){
       // var test = localStorage.getItem("userRole");
       // console.log("User Role from Local Storage: "+test);
       if (localStorage.getItem("userRole") === 'operator'){
         this._router.navigate(['pos']);
       }
     }

     logout() {
         this._service.logout();
     }
}
