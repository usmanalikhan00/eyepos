import { Component, Directive, ViewChild, ElementRef, NgZone, ChangeDetectorRef, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';
import {ProductsService} from '../../services/products.service';
import {refundOrderService} from '../../services/refundorder.service';
import {PosService} from '../../services/pos.service';
import {ordersService} from '../../services/orders.service';
import { Location } from '@angular/common';
import * as _ from 'lodash';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import * as moment from "moment";
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib/index'
PouchDB.plugin(PouchFind)
import {ElectronService} from 'ngx-electron';

@Component({

  selector: 'refund-orders',
  templateUrl: 'components/refundorders/refundorders.html',
  styleUrls: ['components/refundorders/refundorders.css'],
  providers: [ordersService, refundOrderService, ElectronService]

})

export class refundorders{


  public rowsOnPage = 10;
  public sortBy = "orderId";
  public sortOrder = "desc";

  allOrders: any = [];
  allRefunds: any = [];
  filterQuery: string;
  selectedMonth=0;
  TotalSales: any;
  todaySales = 0;
  sortOrderMonthWise: any;
  filterOrderArray = [];
  dataFilter = false;
  queryToPipe: string;
  refundOrders: any;
  refundsAmount: any;
  refundsCount: any = 0
  orderId: any = null

  minDate = new Date(2017, 5, 10);
  maxDate = new Date(2018, 9, 15);
 
  // bsValue: Date = new Date();
  bsRangeValue: any = [];
  daterangepickerModel: Date[];
  allFilteredSales: any = [];
  datepickerModel: any
  datepickerModel2: any

  public refundOrdersDB = new PouchDB('refundorders');
  public cloudantRefundOrdersDB = new PouchDB('https://aajeel.cloudant.com/refundorders', {
    auth: {
      username: 'aajeel',
      password: 'eyeteaco'
    }
  });


  constructor(private _service: ordersService,
              private _refundsService: refundOrderService, 
              private _electronService: ElectronService, 
              private _router: Router, 
              private _location: Location,
              private _ref: ChangeDetectorRef){
    this.sortOrderMonthWise = 0;
  }
  
  ngOnInit(){
    this.getAllRefunds()
  }

  setFilterQuery(){
    // this.dataFilter = true;
    // this.queryToPipe = filterQuery;
    console.log("filter Query Results",this.filterQuery);

  }

  getAllRefunds(){
    var self = this
    var count = 0
    var index = 1
    self.allRefunds = []
    self._refundsService.getAllRefunds().then(function(result){
        result.rows.forEach(function(row){
          row.doc.refundId = index 
          self.allRefunds.push(row.doc)
          count = count + row.doc.refundAmount;
          index++
        });
        self.refundsCount = count
        index = 0
       console.log("RESULR FROM REFUNDS:0000", self.allRefunds, self.refundsCount);
       // console.log(self.itemsToRefund);
    }).catch(function(err){
       console.log(err);
    });
  }

	dateRangeChange(){
		var self = this
    var salesCount = 0;
    self.selectedMonth = 0
		console.log("DATE RANGE CHNAGED:-------", this.datepickerModel, this.datepickerModel2 )
		if (this.datepickerModel && this.datepickerModel2){
			self._service.filterOrders(this.datepickerModel.toISOString(), this.datepickerModel2.toISOString()).then(function(result){
				console.log("RESULTS AFTER FILTER DATES:----", result)
				self.allOrders = []
				result.docs.forEach(function(row){
					self.allOrders.push(row)
	        salesCount = salesCount + row.orderTotal;
				})
	      self.TotalSales = salesCount;
				self.allOrders = _.reverse(self.allOrders)
			}).catch(function(err){
				console.log(err)
			})
		}
	}

	clearFilters(){
		// this.bsRangeValue = null
		this.datepickerModel = null
		this.datepickerModel2 = null
		this.selectedMonth = 0
		this.ngOnInit()
	}

	setToDate($event){
    this.datepickerModel = $event
    // this.minDate = $event.toISOString();
    console.log("FRO DATE CHANGES:--- ", this.datepickerModel, $event);
		
	}


  calcTodaySales(orderDay, orderMonth, orderYear){
    var self = this;
    var todaySales = [];
    var salesCount = 0;
    console.log("ORDER DAY AND MONTH:--- ", orderDay, orderMonth);
    self._service.getTodaySales(orderDay, orderMonth, orderYear).then(function(result){
      // console.log("Today's Sales Result: ", result);
      result.docs.map(function (row) { 
        todaySales.push(row);
        salesCount = salesCount + row.orderTotal;
      });
      console.log("Today's Sales array after mapping: ", todaySales);
      // for (let i=0; i<todaySales.length; i++){
      //   salesCount = salesCount + todaySales[i].orderTotal;
      // }
      self.todaySales = salesCount;
      // console.log("Today's Sales Count: ", self.todaySales);
    }).catch(function (err){
      console.log(err);
    });
  }
  getResultsByMonth(){
    var self = this;
    self.allRefunds = [];
    var refundsCount = 0;
		this.datepickerModel = null
		this.datepickerModel2 = null
    if (self.selectedMonth === 0){
      self.ngOnInit();
    }else{  
      this._refundsService.getResultsByMonth(self.selectedMonth).then(function(result){
        // console.log(result);
        result.docs.map(function (row) { 
          self.allRefunds.push(row);
          refundsCount = refundsCount + row.refundAmount;
        });
        // console.log(result);
        // for (let i=0; i<self.allOrders.length; i++){
        //   salesCount = salesCount + self.allOrders[i].orderTotal;
        // }
        self.refundsCount = refundsCount;
        console.log("TOTAL SALES: ", self.refundsCount);
        // console.log('self.allOrders from orders service', self.allOrders);
      }).catch(function(err){
          console.log(err);
      });
    }
  }

  printInvoice(orderId){
    console.log("ORDER IF FROM CLICKL:=---", orderId)
    console.log(this.orderId)
    // window.print();
    this._electronService.ipcRenderer.send('loadPrintPage', this.orderId);
    // }
    // var win = window.open('','','left=0,top=0,width=250,height=400,toolbar=0,scrollbars=0,status =0');
    // var content = "<html>";
    // content += "<body onload=\"window.print(); window.close();\">";
    // content += document.getElementById("divToPrint").innerHTML ;
    // content += "</body>";
    // content += "</html>";
    // console.log("content by print function", content);
    // win.document.write(content);
    // win.document.close()
  }
  goBack(){
    // this._location.back();
    this._router.navigate(['home']);
  }
}