import { Component, Directive, ViewChild, ElementRef, NgZone, ChangeDetectorRef, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';
import {ProductsService} from '../../services/products.service';
import {refundOrderService} from '../../services/refundorder.service';
import {PosService} from '../../services/pos.service';
import {ordersService} from '../../services/orders.service';
import { Location } from '@angular/common';
import * as _ from 'lodash';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import * as moment from "moment";
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib/index'
PouchDB.plugin(PouchFind)
import {ElectronService} from 'ngx-electron';

@Component({

  selector: 'orders-content',
  templateUrl: 'components/orders/orders.html',
  styleUrls: ['components/orders/orders.css'],
  providers: [ordersService, refundOrderService, ElectronService]

})

export class ordersComponent{


  public rowsOnPage = 5;
  public sortBy = "orderId";
  public sortOrder = "desc";

  allOrders: any = [];
  filterQuery: string;
  selectedMonth: any = null;
  TotalSales: any = 0;
  TotalDiscounts: any = 0;
  todaySales = 0;
  sortOrderMonthWise: any;
  filterOrderArray = [];
  dataFilter = false;
  queryToPipe: string;
  refundOrders: any;
  refundsAmount: any;
  refundsCount: any = 0
  discountsCount: any = 0
  orderId: any = null
  selectedOrder: any = null

  public ordersDB = new PouchDB('orders');
  public cloudantOrdersDB = new PouchDB('https://aajeel.cloudant.com/orders', {
    auth: {
      username: 'aajeel',
      password: 'aajeel@eyetea.co'
    }
  });

  public ibmOrdersDB = new PouchDB('https://360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com/aajeelorders', {
    auth: {
      "username": "360527dd-869f-46ae-be21-c9b27c672b55-bluemix",
      "password": "591b1db46a44e67d7aeaf6323b36ee12b4484d03942c2f5dc942489d99417e9e",
      "host": "360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com",
      "port": 443,
      "url": "https://360527dd-869f-46ae-be21-c9b27c672b55-bluemix:591b1db46a44e67d7aeaf6323b36ee12b4484d03942c2f5dc942489d99417e9e@360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com"
    }
    // auth: {
    //   username: 'arsie108@gmail.com',
    //   password: 'Admin123@'
    // }
  });

  public refundOrdersDB = new PouchDB('refundorders');
  public cloudantRefundOrdersDB = new PouchDB('https://aajeel.cloudant.com/refundorders', {
    auth: {
      username: 'aajeel',
      password: 'aajeel@eyetea.co'
    }
  });
  public ibmRefundOrdersDB = new PouchDB('https://360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com/aajeelrefundorders', {
    auth: {
      "username": "360527dd-869f-46ae-be21-c9b27c672b55-bluemix",
      "password": "591b1db46a44e67d7aeaf6323b36ee12b4484d03942c2f5dc942489d99417e9e",
      "host": "360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com",
      "port": 443,
      "url": "https://360527dd-869f-46ae-be21-c9b27c672b55-bluemix:591b1db46a44e67d7aeaf6323b36ee12b4484d03942c2f5dc942489d99417e9e@360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com"
    }
    // auth: {
    //   username: 'arsie108@gmail.com',
    //   password: 'Admin123@'
    // }
  });



  minDate = new Date(2017, 5, 10);
  maxDate = new Date(2018, 9, 15);
 
  // bsValue: Date = new Date();
  bsRangeValue: any = [];
  daterangepickerModel: Date[];
  allFilteredSales: any = [];
  datepickerModel: any
  datepickerModel2: any


// PolarArea
  public polarAreaChartLabels:string[] = ['Revenue'];
  public polarAreaChartData:number[] = [300];
  public polarAreaLegend:boolean = true;
 
  public polarAreaChartType:string = 'polarArea';
 
  // events

  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };

  barChartLabels:string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  barChartType:string = 'line';
  barChartLegend:boolean = true;  

 
  barChartData:any[] = [
    {data: [300,200,100,50,90,120,150,300,500,300,650,450], label: 'Sales'},
    {data: [28, 48, 40, 19, 86, 27, 90, 87, 120,65,89,56], label: 'Revenue'}
  ];

 // Doughnut
  public doughnutChartLabels:string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  // public doughnutChartLabels:string[] = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  public doughnutChartData:number[] = [300,200,100,50,90,120,150,300,500,300,650,450];
  public doughnutChartType:string = 'pie';

  barChartLabels2:string[] = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24','25','26','27','28','29','30','31'];
  barChartData2:any[] = [
    {data: [300,200,100,50,90,120,150,300,500,300,650,450,300,200,100,50,90,120,150,300,650,450,300,200,100,50,90,120,150,300], label: 'Sales'},
    {data: [300,200,100,50,90,120,150,300,500,300,650,450,300,200,100,50,90,120,150,300,650,450,300,200,100,50,90,120,150,300], label: 'Revenue'}
    // {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
  ];

  barChartLabels3:string[] = [];
  barChartData3:any[] = [
    {data: [300], label: 'Sales'}
    // {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
  ];
  barChartType3:string = 'bar';
  barChartLegend3:boolean = true;



  max: number = 200;
  // showWarning: boolean;
  dynamic: number;
  // type: string;
  defaultProgressBar: boolean = false;
  selectedYear: any = null


  ordersPageSize: any = 10;
  ordersPageLimit: any = null;
  ordersPageSkip: any = null;
  ordersStartKey: any = null;
  ordersEndKey: any = null;
  ordersFirstPageOptions: any = {'limit':this.ordersPageSize};
  ordersNextPageOptions: any = {'limit':this.ordersPageSize, 'skip':1, 'startkey':this.ordersStartKey};


  constructor(private _service: ordersService,
              private _refundsService: refundOrderService, 
              private _electronService: ElectronService, 
              private _router: Router, 
              private _location: Location,
              private _ref: ChangeDetectorRef){
    this.sortOrderMonthWise = 0;
    // this.syncDb(); 
    this.replicateDatabase() 
  }
  
  public chartClicked(e:any):void {
    console.log(e);
    console.log(e.event.srcElement.value);
  }
 
  public chartHovered(e:any):void {
    console.log(e);
  }

  private syncDb(){
    var self = this;
    self.ordersDB.sync(self.cloudantOrdersDB, {
      live: true,
      retry: true
    }).on('change', function(change){
      console.log("Changes from db sync", change);
      self.TotalSales = 0
      self.TotalDiscounts = 0
      var salesCount = 0;
      var discountsCount = 0;
      self.allOrders = [];
      self._service.getAllOrders().then(function (result) {
        result.rows.map(function (row) { 
          self.allOrders.push(row.doc);
          salesCount = salesCount + row.doc.orderTotal;
          if (row.doc.orderDisount != null){
            discountsCount = discountsCount + row.doc.orderDiscount
          }
        });
        // for (let i=0; i<self.allOrders.length; i++){
        //   salesCount = salesCount + self.allOrders[i].orderTotal;
        // }
        self.TotalSales = salesCount;
        self.max = salesCount
        self.TotalDiscounts = discountsCount
        console.log("TOTAL SALES & TOTAL DISOCUNTS: ", self.TotalSales, self.TotalDiscounts);
        self.calcTodaySales(moment(new Date()).format('D'), moment(new Date()).format('MM'), moment(new Date()).format('YYYY'));
      }).catch(function (err) {
        console.log(err);
      });
    }).on('paused', function (info) {
      console.log("Info from Paused sync:", info);
    }).on('active', function () {
      console.log("Info from active sync:");
    }).on('error', function(err){
      console.log("Changes from db sync", err);
    });
    var db = self.refundOrdersDB.sync(self.cloudantRefundOrdersDB, {
              live: true,
              retry: true
    }).on('change', function (change) {
      // this.updateDocs();
      // this.changes(change);
      // self.getAllRefunds()
      console.log("Info from Change sync EFUNDS:", change);
      var count = 0
      self._refundsService.getAllRefunds().then(function(result){
          result.docs.map(function (row) { 
            count = count + row.refundAmount;
          });
          self.refundsCount = count
         // console.log(self.orderToRefund);
         // console.log(self.itemsToRefund);
      }).catch(function(err){
         console.log(err);
      });
      // self.updateDocs();
      // replication was paused, usually because of a lost connection
    }).on('paused', function (info) {
      console.log("Info from Paused sync:", info);
      // replication was resumed
    }).on('active', function () {
      // yo, something changed!
      console.log("Info from active sync:");
    }).on('error', function (err) {
      console.log("Info from Error sync:", err);
      // totally unhandled error (shouldn't happen)
    });
  } 

  replicateDatabase(){
    var self = this
    var opts = { live: true, retry: true };
    self.ordersDB.replicate.from(self.ibmOrdersDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM CLOUDANT ONE-WAY ORDERS REPLICATION:--", info)
      self.ordersDB.sync(self.ibmOrdersDB, opts).on('change', function (info) {
        console.log("CHANGE EVENT FROM TWO-WAY ORDERS SYNC:--", info)
        // if (info.direction === 'pull' || info.direction === 'push'){
        //   // self.()
        // }
        // self.TotalSales = 0
        // self.TotalDiscounts = 0
        // var salesCount = 0;
        // var discountsCount = 0;
        // self.allOrders = [];
        // self._service.getAllOrders().then(function (result) {
        //   result.rows.map(function (row) { 
        //     self.allOrders.push(row.doc);
        //     salesCount = salesCount + row.doc.orderTotal;
        //     if (row.doc.orderDisount != null){
        //       discountsCount = discountsCount + row.doc.orderDiscount
        //     }
        //   });
        //   // for (let i=0; i<self.allOrders.length; i++){
        //   //   salesCount = salesCount + self.allOrders[i].orderTotal;
        //   // }
        //   self.TotalSales = salesCount;
        //   self.max = salesCount
        //   self.TotalDiscounts = discountsCount
        //   console.log("TOTAL SALES & TOTAL DISOCUNTS: ", self.TotalSales, self.TotalDiscounts);
        //   // console.log("TOTAL SALES: ", self.TotalSales);
        //   self.calcTodaySales(moment(new Date()).format('D'), moment(new Date()).format('MM'), moment(new Date()).format('YYYY'));
        // }).catch(function (err) {
        //   console.log(err);
        // });
      }).on('paused', function (err) {
        console.log("PAUSE EVENT FROM TWO-WAY SYNC ORDERS", err)
      }).on('active', function () {
        console.log("ACTIVE ACTIVE FROM TWO-WAY ORDERS SYNC!!")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR FROM SYNC ORDERS!!", err)
      })
      self.refundOrdersDB.replicate.from(self.ibmRefundOrdersDB).on('complete', function(info) {
        console.log("COMPLETE EVENT FROM CLOUDANT ONE-WAY REFUND ORDERS REPLICATION:--", info)
        self.refundOrdersDB.sync(self.ibmRefundOrdersDB, opts).on('change', function (info) {
          console.log("CHANGE EVENT FROM TWO-WAY REFUND ORDERS SYNC:--", info)
          // if (info.direction === 'pull' || info.direction === 'push'){
          //   // self.()
          // }
          // var count = 0
          // self._refundsService.getAllRefunds().then(function(result){
          //     result.docs.map(function (row) { 
          //       count = count + row.refundAmount;
          //     });
          //     self.refundsCount = count
          //    // console.log(self.orderToRefund);
          //    // console.log(self.itemsToRefund);
          // }).catch(function(err){
          //    console.log(err);
          // });
        }).on('paused', function (err) {
          console.log("PAUSE EVENT FROM TWO-WAY SYNC REFUND ORDERS", err)
        }).on('active', function () {
          console.log("ACTIVE ACTIVE FROM TWO-WAY REFUND ORDERS SYNC!!")
        }).on('denied', function (err) {
          console.log("DENIED DENIED !!", err)
        }).on('complete', function (info) {
          console.log("COMPLETED !!", info)
        }).on('error', function (err) {
          console.log("ERROR ERROR FROM SYNC REFUND ORDERS!!", err)
        })
      }).on('error', function (err) {
        console.log("ERROR ERROR FROM REPLICATION CLOUDANT REFUND ORDERS !!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR FROM REPLICATION CLOUDANT ORDERS !!", err)
    })
  }

  getOrderId(order){
    console.log("SELECTED ORDER ID IS:--", order._id)
    this.selectedOrder = order;
  }

  ngOnInit(){
    var self = this;
    var salesCount = 0;
    var discountsCount = 0;
    var refundsCount = 0;
    self.TotalSales = 0
    self.TotalDiscounts = 0
    self.allOrders = [];
    self.refundOrders = [];
    self._service.getAllOrders().then(function (result) {
      // console.log("RESULT FROM ALL ORDERS:---", result)
      result.rows.map(function (row) { 
        self.allOrders.push(row.doc);
        salesCount = salesCount + row.doc.orderTotal;
        if (row.doc.orderDiscount != null){
          discountsCount = discountsCount + row.doc.orderDiscount
        }
      });
      // self.allOrders = _.reverse(self.allOrders)
      self.TotalSales = salesCount;
      self.max = salesCount;
      self.dynamic = salesCount
      self.TotalDiscounts = discountsCount
      // console.log("TOTAL SALES & TOTAL DISOCUNTS: ", self.TotalSales, self.TotalDiscounts);
      self.calcTodaySales(moment(new Date()).format('D'), moment(new Date()).format('MMMM'), moment(new Date()).format('YYYY'));
      self.makeChart(self.allOrders)
      self._refundsService.getAllRefunds().then(function(result){
        // console.log("result from refunds: ", result);
        result.rows.map(function (row) { 
          self.refundOrders.push(row.doc);
	        refundsCount = refundsCount + row.doc.refundAmount;
	      });
	      // console.log("REFUNDED ORDERS: ", self.refundOrders);
	      self.refundsAmount = refundsCount;
	      // console.log("TOTAL REFUNDS: ", self.refundsAmount);
	    }).catch(function(err){
	      console.log("error from refunds: ", err);
	    });
    }).catch(function (err) {
      console.log(err);
    });
    // this.firstOrderPage()
  }
  setFilterQuery(){
    // this.dataFilter = true;
    // this.queryToPipe = filterQuery;
    console.log("filter Query Results",this.filterQuery);

  }


  makeChart(allOrders){
    var janSales: any = 0
    var janCount: any = 0
    var febSales: any = 0
    var febCount: any = 0
    var marSales: any = 0
    var marCount: any = 0
    var aprSales: any = 0
    var aprCount: any = 0
    var maySales: any = 0
    var mayCount: any = 0
    var junSales: any = 0
    var junCount: any = 0
    var julSales: any = 0
    var julCount: any = 0
    var augSales: any = 0
    var augCount: any = 0
    var sepSales: any = 0
    var sepCount: any = 0
    var octSales: any = 0
    var octCount: any = 0
    var novSales: any = 0
    var novCount: any = 0
    var decSales: any = 0
    var decCount: any = 0

    for (let item of allOrders){
      if (item.orderMonth === "January"){
        janSales = janSales + 1
        janCount = janCount + item.orderTotal
      }
      if (item.orderMonth === "February"){
        febSales = febSales + 1
        febCount = febCount + item.orderTotal
      }
      if (item.orderMonth === "March"){
        marSales = marSales + 1
        marCount = marCount + item.orderTotal

      }
      if (item.orderMonth === "April"){
        aprSales = aprSales + 1
        aprCount = aprCount + item.orderTotal
      }
      if (item.orderMonth === "May"){
        maySales = maySales + 1
        mayCount = marCount + item.orderTotal
      }
      if (item.orderMonth === "June"){
        junSales = junSales + 1
        junCount = junCount + item.orderTotal
      }
      if (item.orderMonth === "July"){
        julSales = julSales + 1
        julCount = julCount + item.orderTotal
      }
      if (item.orderMonth === "August"){
        augSales = augSales + 1
        augCount = augCount + item.orderTotal
      }
      if (item.orderMonth === "September"){
        sepSales = sepSales + 1
        sepCount = sepCount + item.orderTotal
      }
      if (item.orderMonth === "October"){
        octSales = octSales + 1
        octCount = octCount + item.orderTotal
      }
      if (item.orderMonth === "November"){
        novSales = novSales + 1
        novCount = novCount + item.orderTotal
      }
      if (item.orderMonth === "December"){
        decSales = decSales + 1
        decCount = decCount + item.orderTotal
      }

    }
    // this.polarAreaChartLabels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
    // this.polarAreaChartData = [janSales, febSales, marSales, aprSales, maySales, junSales, julSales, augSales, sepSales, octSales, novSales, decSales]
    this.barChartData = [
      {data:[janSales, febSales, marSales, aprSales, maySales, junSales, julSales, augSales, sepSales, octSales, novSales, decSales], label:'Sales'},
      {data:[janCount, febCount, marCount, aprCount, mayCount, junCount, julCount, augCount, sepCount, octCount, novCount, decCount], label:'Revenue'}
    ]
    this.doughnutChartData = [janCount, febCount, marCount, aprCount, mayCount, junCount, julCount, augCount, sepCount, octCount, novCount, decCount]
    // console.log("POLAR AREA CHART LABELS:---", this.polarAreaChartLabels);
    // console.log("POLAR AREA CHART DATA:---", this.polarAreaChartData);
    // console.log("DOUGHNUT AREA CHART DATA:---", this.doughnutChartData);

  }

  getAllRefunds(){
    var self = this
    var count = 0
    self._refundsService.getAllRefunds().then(function(result){
        result.docs.map(function (row) { 
          count = count + row.refundAmount;
        });
        self.refundsCount = count
       // console.log(self.orderToRefund);
       // console.log(self.itemsToRefund);
    }).catch(function(err){
       console.log(err);
    });
  }

	dateRangeChange(){
		var self = this
    var salesCount = 0;
    var discountsCount = 0;
    self.selectedMonth = null
    self.defaultProgressBar = false
		// console.log("DATE RANGE CHNAGED:-------", this.datepickerModel, this.datepickerModel2 )
		if (this.datepickerModel && this.datepickerModel2){
			self._service.filterOrders(this.datepickerModel.toISOString(), this.datepickerModel2.toISOString()).then(function(result){
				// console.log("RESULTS AFTER FILTER DATES:----", result)
				self.allOrders = []
				result.docs.forEach(function(row){
					self.allOrders.push(row)
	        salesCount = salesCount + row.orderTotal;
          if (row.orderDiscount !=  null){
            discountsCount = discountsCount + row.orderDiscount
          }
				})
	      self.TotalSales = salesCount;
        self.dynamic = salesCount
        self.TotalDiscounts = discountsCount
        // self.defaultProgressBar = false
				self.allOrders = _.reverse(self.allOrders)
			}).catch(function(err){
				console.log(err)
			})
		}
	}

	clearFilters(){
		// this.bsRangeValue = null
		this.datepickerModel = null
		this.datepickerModel2 = null
    this.selectedMonth = null
		this.selectedYear = null
    this.defaultProgressBar = false
		this.ngOnInit()
	}

	setToDate($event){
    this.datepickerModel = $event
    // this.minDate = $event.toISOString();
    console.log("FRO DATE CHANGES:--- ", this.datepickerModel, $event);
    
  }

  removeOrder(order){
    var self = this
    console.log("ORDER TO DELETE:--- ", order);
    self._service.removeOrder(order).then(function(result){
      console.log("RESULT AFTER DLETING ORDER:---", result)
    }).catch(function(err){
      console.log(err)
    })
  }

  calcTodaySales(orderDay, orderMonth, orderYear){
    var self = this;
    var todaySales = [];
    var salesCount = 0;
    var discountsCount = 0;
    // console.log("ORDER DAY AND MONTH, YEAR:--- ", orderDay, orderMonth, orderYear);
    self._service.getTodaySales(orderDay, orderMonth, orderYear).then(function(result){
      // console.log("Today's Sales Result: ", result);
      result.docs.map(function (row) { 
        todaySales.push(row);
        salesCount = salesCount + row.orderTotal;
        if (row.orderDiscount != null){
          discountsCount = discountsCount + row.orderDiscount
        }
      });
      // console.log("Today's Sales array after mapping: ", todaySales);
      // for (let i=0; i<todaySales.length; i++){
      //   salesCount = salesCount + todaySales[i].orderTotal;
      // }
      self.todaySales = salesCount;
      self.discountsCount = discountsCount;
      // console.log("Today's Sales Count & DISOCUNT COUNT: ", self.todaySales, self.discountsCount);
    }).catch(function (err){
      console.log(err);
    });
  }
  getResultsByMonth(){
    var self = this;
    self.defaultProgressBar = true
    self.allOrders = [];
    var salesCount = 0;
    self.TotalSales = 0;
    this.datepickerModel = null
    this.datepickerModel2 = null
    if (self.selectedMonth === null && this.selectedYear === null){
      self.ngOnInit();
      self.defaultProgressBar = false
    }
    if (self.selectedMonth !== null && self.selectedYear === null){  
      this._service.getResultsByMonth(self.selectedMonth).then(function(result){
        // console.log(result);
        result.docs.map(function (row) { 
          self.allOrders.push(row);
          salesCount = salesCount + row.orderTotal;
        });
        // console.log(result);
        // for (let i=0; i<self.allOrders.length; i++){
        //   salesCount = salesCount + self.allOrders[i].orderTotal;
        // }
        self.TotalSales = salesCount;
        self.dynamic = salesCount
        self.makeMonthChart(self.allOrders)
        // console.log("TOTAL SALES: ", self.TotalSales);
        // console.log('self.allOrders MONTH CHANGE', self.allOrders);
      }).catch(function(err){
          console.log(err);
      });
    }
    if (self.selectedMonth === null && self.selectedYear !== null){  
      this._service.getResultsByYear(self.selectedYear).then(function(result){
        // console.log(result);
        result.docs.map(function (row) { 
          self.allOrders.push(row);
          salesCount = salesCount + row.orderTotal;
        });
        // console.log(result);
        // for (let i=0; i<self.allOrders.length; i++){
        //   salesCount = salesCount + self.allOrders[i].orderTotal;
        // }
        self.TotalSales = salesCount;
        self.dynamic = salesCount
        self.makeChart(self.allOrders)
        // console.log("TOTAL SALES: ", self.TotalSales);
        self.defaultProgressBar = false
        // console.log('self.allOrders MONTH CHANGE', self.allOrders);
      }).catch(function(err){
          console.log(err);
      });
    }
    if (self.selectedMonth !== null && self.selectedYear !== null){  
      this._service.getResultsByMonthYear(self.selectedYear, self.selectedMonth).then(function(result){
        // console.log(result);
        result.docs.map(function (row) { 
          self.allOrders.push(row);
          salesCount = salesCount + row.orderTotal;
        });
        // console.log(result);
        // for (let i=0; i<self.allOrders.length; i++){
        //   salesCount = salesCount + self.allOrders[i].orderTotal;
        // }
        self.TotalSales = salesCount;
        self.dynamic = salesCount
        self.makeMonthChart(self.allOrders)
        // console.log("TOTAL SALES: ", self.TotalSales);
        // console.log('self.allOrders MONTH CHANGE YEAR AND MONTH NOT NULL:-------', self.allOrders);
      }).catch(function(err){
          console.log(err);
      });
    }


  }

  makeMonthChart(allOrders){
    var oneSales: any = 0
    var oneCount: any = 0
    var twoSales: any = 0
    var twoCount: any = 0
    var threeSales: any = 0
    var threeCount: any = 0
    var fourSales: any = 0
    var fourCount: any = 0
    var fiveSales: any = 0
    var fiveCount: any = 0
    var sixSales: any = 0
    var sixCount: any = 0
    var sevenSales: any = 0
    var sevenCount: any = 0
    var eightSales: any = 0
    var eightCount: any = 0
    var nineSales: any = 0
    var nineCount: any = 0
    var tenSales: any = 0
    var tenCount: any = 0
    var eleSales: any = 0
    var eleCount: any = 0
    var tweSales: any = 0
    var tweCount: any = 0
    var thirSales: any = 0
    var thirCount: any = 0
    var fourteenSales: any = 0
    var fourteenCount: any = 0
    var fiftSales: any = 0
    var fiftCount: any = 0
    var sixteenSales: any = 0
    var sixteenCount: any = 0
    var seventeenSales: any = 0
    var seventeenCount: any = 0
    var eigSales: any = 0
    var eigCount: any = 0
    var ninSales: any = 0
    var ninCount: any = 0
    var twentySales: any = 0
    var twentyCount: any = 0
    var twentyOneSales: any = 0
    var twentyOneCount: any = 0
    var twentyTwoSales: any = 0
    var twentyTwoCount: any = 0
    var twentyThreeSales: any = 0
    var twentyThreeCount: any = 0
    var twentyFourSales: any = 0
    var twentyFourCount: any = 0
    var twentyFiveSales: any = 0
    var twentyFiveCount: any = 0
    var twentySixSales: any = 0
    var twentySixCount: any = 0
    var twentySevenSales: any = 0
    var twentySevenCount: any = 0
    var twentyEightSales: any = 0
    var twentyEightCount: any = 0
    var twentyNineSales: any = 0
    var twentyNineCount: any = 0
    var thirtySales: any = 0
    var thirtyCount: any = 0
    var thirtyOneSales: any = 0
    var thirtyOneCount: any = 0




    for (let item of allOrders){
      if (item.orderDay == "1"){
        oneSales = oneSales + 1
        oneCount = oneCount + item.orderTotal
      }
      if (item.orderDay == "2"){
        twoSales = twoSales + 1
        twoCount = twoCount + item.orderTotal
      }
      if (item.orderDay == "3"){
        threeSales = threeSales + 1
        threeCount = threeCount + item.orderTotal

      }
      if (item.orderDay == "4"){
        fourSales = fourSales + 1
        fourCount = fourCount + item.orderTotal
      }
      if (item.orderDay == "5"){
        fiveSales = fiveSales + 1
        fiveCount = fiveCount + item.orderTotal
      }
      if (item.orderDay == "6"){
        sixSales = sixSales + 1
        sixCount = sixCount + item.orderTotal
      }
      if (item.orderDay == "7"){
        sevenSales = sevenSales + 1
        sevenCount = sevenCount + item.orderTotal
      }
      if (item.orderDay == "8"){
        eightSales = eightSales + 1
        eightCount = eightCount + item.orderTotal
      }
      if (item.orderDay == "9"){
        nineSales = nineSales + 1
        nineCount = nineCount + item.orderTotal
      }
      if (item.orderDay == "10"){
        tenSales = tenSales + 1
        tenCount = tenCount + item.orderTotal
      }
      if (item.orderDay == "11"){
        eleSales = eleSales + 1
        eleCount = eleCount + item.orderTotal
      }
      if (item.orderDay == "12"){
        tweSales = tweSales + 1
        tweCount = tweCount + item.orderTotal
      }
      if (item.orderDay == "13"){
        thirSales = thirSales + 1
        thirCount = thirCount + item.orderTotal
      }
      if (item.orderDay == "14"){
        fourteenSales = fourteenSales + 1
        fourteenCount = fourteenCount + item.orderTotal
      }
      if (item.orderDay == "15"){
        fiftSales = fiftSales + 1
        fiftCount = fiftCount + item.orderTotal
      }
      // sixteenCount = 0
      // sixteenSales = 0
      if (item.orderDay == "16"){
        sixteenSales = sixteenSales + 1
        sixteenCount = sixteenCount + item.orderTotal
      }
      if (item.orderDay == "17"){
        seventeenSales = seventeenSales + 1
        seventeenCount = seventeenCount + item.orderTotal
      }
      if (item.orderDay == "18"){
        eigSales = eigSales + 1
        eigCount = eigCount + item.orderTotal
      }
      if (item.orderDay == "19"){
        ninSales = ninSales + 1
        ninCount = ninCount + item.orderTotal
      }
      if (item.orderDay == "20"){
        twentySales = twentySales + 1
        twentyCount = twentyCount + item.orderTotal
      }
      if (item.orderDay == "21"){
        twentyOneSales = twentyOneSales + 1
        twentyOneCount = twentyOneCount + item.orderTotal
      }
      if (item.orderDay == "22"){
        twentyTwoSales = twentyTwoSales + 1
        twentyTwoCount = twentyTwoCount + item.orderTotal
      }
      if (item.orderDay == "23"){
        twentyThreeSales = twentyThreeSales + 1
        twentyThreeCount = twentyThreeCount + item.orderTotal
      }
      if (item.orderDay == "24"){
        twentyFourSales = twentyFourSales + 1
        twentyFourCount = twentyFourCount + item.orderTotal
      }
      if (item.orderDay == "25"){
        twentyFiveSales = twentyFiveSales + 1
        twentyFiveCount = twentyFiveCount + item.orderTotal
      }
      if (item.orderDay == "26"){
        twentySixSales = twentySixSales + 1
        twentySixCount = twentySixCount + item.orderTotal
      }
      if (item.orderDay == "27"){
        twentySevenSales = twentySevenSales + 1
        twentySevenCount = twentySevenCount + item.orderTotal
      }
      if (item.orderDay == "28"){
        twentyEightSales = twentyEightSales + 1
        twentyEightCount = twentyEightCount + item.orderTotal
      }
      if (item.orderDay == "29"){
        twentyNineSales = twentyNineSales + 1
        twentyNineCount = twentyNineCount + item.orderTotal
      }
      if (item.orderDay == "30"){
        thirtySales = thirtySales + 1
        thirtyCount = thirtyCount + item.orderTotal
      }
      if (item.orderDay == "31"){
        thirtyOneSales = thirtyOneSales + 1
        thirtyOneCount = thirtyOneCount + item.orderTotal
      }

    }
    // this.barChartLabels = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20']
    this.barChartData2 = [
      {data: [oneSales, twoSales, threeSales, fourSales, fiveSales, sixSales, sevenSales, eightSales, nineSales, tenSales, 
        eleSales, tweSales, thirSales, fourteenSales, fiftSales, sixteenSales, seventeenSales, eigSales, ninSales, twentySales,
        twentyOneSales, twentyTwoSales, twentyThreeSales, twentyFourSales, twentyFiveSales, twentySixSales, twentySevenSales, twentyEightSales,
        twentyNineSales, thirtySales, thirtyOneSales], label:'Sales'},      
      {data: [oneCount, twoCount, threeCount, fourCount, fiveCount, sixCount, sevenCount, eightCount, nineCount, tenCount, 
        eleCount, tweCount, thirCount, fourteenCount, fiftCount, sixteenCount, seventeenCount, eigCount, ninCount, twentyCount,
        twentyOneCount, twentyTwoCount, twentyThreeCount, twentyFourCount, twentyFiveCount, twentySixCount, twentySevenCount, twentyEightCount,
        twentyNineCount, thirtyCount, thirtyOneCount], label:'Revenue'}
    ]
    this.barChartData3 = [
      {data:[allOrders.length],label:'Sales'}
    ]
    this.selectedMonth = this.selectedMonth.toString().replace(/['"]+/g, '')
    this.barChartLabels3 = [this.selectedMonth]
    // this.bar

    this.polarAreaChartData = [this.TotalSales]
    // console.log("POLAR AREA CHART LABELS:---", this.polarAreaChartLabels);
    // console.log("POLAR AREA CHART DATA:---", this.polarAreaChartData);
    // console.log("DOUGHNUT AREA CHART DATA:---", this.doughnutChartData);
    // console.log("BAR CHART DATA:---", this.barChartData);
    // console.log("BAR CHART LABELS:---", this.barChartLabels);
    // console.log("BAR CHART DATA 3:---", this.barChartData3);
    // console.log("BAR CHART LABELS 3:---", this.barChartLabels3);
    
  }


  printInvoice(orderId){
    console.log("ORDER IF FROM CLICKL:=---", orderId)
    console.log(this.orderId)
    // window.print();
    this._electronService.ipcRenderer.send('loadPrintPage', orderId);
    // this._router.navigate(['transactions', this.orderId])
    // }
    // var win = window.open('','','left=0,top=0,width=250,height=400,toolbar=0,scrollbars=0,status =0');
    // var content = "<html>";
    // content += "<body onload=\"window.print(); window.close();\">";
    // content += document.getElementById("divToPrint").innerHTML ;
    // content += "</body>";
    // content += "</html>";
    // console.log("content by print function", content);
    // win.document.write(content);
    // win.document.close()
  }

  goToRefunds(){
    this._router.navigate(['refundorders'])
  }
  
  goBack(){
    // this._location.back();
    this._router.navigate(['home']);
  }
}