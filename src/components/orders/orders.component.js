"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var refundorder_service_1 = require("../../services/refundorder.service");
var orders_service_1 = require("../../services/orders.service");
var common_1 = require("@angular/common");
var _ = require("lodash");
var moment = require("moment");
var PouchDB = require("pouchdb/dist/pouchdb");
var PouchFind = require("pouchdb-find/lib/index");
PouchDB.plugin(PouchFind);
var ordersComponent = (function () {
    function ordersComponent(_service, _refundsService, _router, _location, _ref) {
        this._service = _service;
        this._refundsService = _refundsService;
        this._router = _router;
        this._location = _location;
        this._ref = _ref;
        this.rowsOnPage = 10;
        this.sortBy = "orderId";
        this.sortOrder = "desc";
        this.allOrders = [];
        this.selectedMonth = 0;
        this.todaySales = 0;
        this.filterOrderArray = [];
        this.dataFilter = false;
        this.ordersDB = new PouchDB('orders');
        this.cloudantOrdersDB = new PouchDB('https://aajeel.cloudant.com/orders', {
            auth: {
                username: 'aajeel',
                password: 'eyeteaco'
            }
        });
        this.minDate = new Date(2017, 5, 10);
        this.maxDate = new Date(2018, 9, 15);
        this.bsRangeValue = [];
        this.allFilteredSales = [];
        this.sortOrderMonthWise = 0;
        this.syncDb();
    }
    ordersComponent.prototype.syncDb = function () {
        var self = this;
        var salesCount = 0;
        self.ordersDB.sync(self.cloudantOrdersDB, {
            live: true,
            retry: true
        }).on('change', function (change) {
            console.log("Changes from db sync", change);
            self.allOrders = [];
            self._service.getAllOrders().then(function (result) {
                result.rows.map(function (row) {
                    self.allOrders.push(row.doc);
                    salesCount = salesCount + row.doc.orderTotal;
                });
                self.TotalSales = salesCount;
                console.log("TOTAL SALES: ", self.TotalSales);
            }).catch(function (err) {
                console.log(err);
            });
        }).on('paused', function (info) {
            console.log("Info from Paused sync:", info);
        }).on('active', function () {
            console.log("Info from active sync:");
        }).on('error', function (err) {
            console.log("Changes from db sync", err);
        });
    };
    ordersComponent.prototype.ngOnInit = function () {
        var self = this;
        var salesCount = 0;
        var refundsCount = 0;
        self.allOrders = [];
        self.refundOrders = [];
        self._service.getAllOrders().then(function (result) {
            console.log("RESULT FROM ALL ORDERS:---", result);
            result.rows.map(function (row) {
                self.allOrders.push(row.doc);
                salesCount = salesCount + row.doc.orderTotal;
            });
            self.allOrders = _.reverse(self.allOrders);
            self.TotalSales = salesCount;
            self.calcTodaySales(moment(new Date()).format('D'), moment(new Date()).format('MMMM'));
            console.log("TOTAL SALES: ", self.TotalSales);
            self._refundsService.getAllRefunds().then(function (result) {
                console.log("result from refunds: ", result);
                result.rows.map(function (row) {
                    self.refundOrders.push(row.doc);
                    refundsCount = refundsCount + row.doc.refundAmount;
                });
                console.log("REFUNDED ORDERS: ", self.refundOrders);
                self.refundsAmount = refundsCount;
                console.log("TOTAL REFUNDS: ", self.refundsAmount);
            }).catch(function (err) {
                console.log("error from refunds: ", err);
            });
        }).catch(function (err) {
            console.log(err);
        });
    };
    ordersComponent.prototype.setFilterQuery = function () {
        console.log("filter Query Results", this.filterQuery);
    };
    ordersComponent.prototype.dateRangeChange = function () {
        var self = this;
        var salesCount = 0;
        console.log("DATE RANGE CHNAGED:-------", this.datepickerModel, this.datepickerModel2);
        if (this.datepickerModel && this.datepickerModel2) {
            self._service.filterOrders(this.datepickerModel.toISOString(), this.datepickerModel2.toISOString()).then(function (result) {
                console.log("RESULTS AFTER FILTER DATES:----", result);
                self.allOrders = [];
                result.docs.forEach(function (row) {
                    self.allOrders.push(row);
                    salesCount = salesCount + row.orderTotal;
                });
                self.TotalSales = salesCount;
                self.allOrders = _.reverse(self.allOrders);
            }).catch(function (err) {
                console.log(err);
            });
        }
        else {
            this.ngOnInit();
        }
    };
    ordersComponent.prototype.clearFilters = function () {
        this.ngOnInit();
    };
    ordersComponent.prototype.calcTodaySales = function (orderDay, orderMonth) {
        var self = this;
        var todaySales = [];
        var salesCount = 0;
        console.log("ORDER DAY AND MONTH:--- ", orderDay, orderMonth);
        self._service.getTodaySales(orderDay, orderMonth).then(function (result) {
            result.docs.map(function (row) {
                todaySales.push(row);
                salesCount = salesCount + row.orderTotal;
            });
            console.log("Today's Sales array after mapping: ", todaySales);
            self.todaySales = salesCount;
        }).catch(function (err) {
            console.log(err);
        });
    };
    ordersComponent.prototype.getResultsByMonth = function () {
        var self = this;
        self.allOrders = [];
        var salesCount = 0;
        if (self.selectedMonth === 0) {
            self.ngOnInit();
        }
        else {
            this._service.getResultsByMonth(self.selectedMonth).then(function (result) {
                result.docs.map(function (row) {
                    self.allOrders.push(row);
                    salesCount = salesCount + row.orderTotal;
                });
                self.TotalSales = salesCount;
                console.log("TOTAL SALES: ", self.TotalSales);
            }).catch(function (err) {
                console.log(err);
            });
        }
    };
    ordersComponent.prototype.goBack = function () {
        this._router.navigate(['home']);
    };
    ordersComponent = __decorate([
        core_1.Component({
            selector: 'orders-content',
            templateUrl: 'components/orders/orders.html',
            styleUrls: ['components/orders/orders.css'],
            providers: [orders_service_1.ordersService, refundorder_service_1.refundOrderService]
        }),
        __metadata("design:paramtypes", [orders_service_1.ordersService,
            refundorder_service_1.refundOrderService,
            router_1.Router,
            common_1.Location,
            core_1.ChangeDetectorRef])
    ], ordersComponent);
    return ordersComponent;
}());
exports.ordersComponent = ordersComponent;
//# sourceMappingURL=orders.component.js.map