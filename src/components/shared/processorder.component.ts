import { Component, Input, ViewChild, ElementRef, NgZone, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';
import {ProductsService} from '../../services/products.service';
import { User, Product } from '../../models/model-index';
import { Location } from '@angular/common';
import {Headers, Http} from '@angular/http';
import {HostListener} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import * as PouchDB  from 'pouchdb';

@Component({
  selector: 'process-order',
  templateUrl: 'components/shared/processorder.html',
  styleUrls: ['components/shared/processorder.css']
  // template: `<h2>Process Order Component</h2>
    //    <span>{{orderId}}:{{orderTotal}}</span>  
    //   `
  // providers: [AuthenticationService, ProductsService]
  // host: {
  //       '(document:keypress)': 'handleKeyboardEvents($event)'
  //   }
})
export class processOrder{
	@Input('') orderTotal: number = 0;
	@Input('') orderId: number = 0;
	constructor(){}
}