import * as _ from "lodash";
import * as moment from "moment";
import {Pipe, PipeTransform} from "@angular/core";


@Pipe({
    name: "orderFilter"
})
export class ordersFilterPipe implements PipeTransform {
    transform(array: any[], query: string): any {
        if (query) {
            console.log("In if of filter pipe", array);
            return _.filter(array, row => row.orderId.toString().indexOf(query) > -1);
        }
        // if (query && callFrom == false) {
        //   console.log("in false condition");
        //     return _.filter(array, row => moment(row._id).format('MMMM Do YYYY, h:mm:ss a').indexOf(query) > -1);
        // }
        console.log("Array In else of filter pipe", array);
        return array;
    }
}