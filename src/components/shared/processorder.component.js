"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
require("rxjs/add/operator/toPromise");
var processOrder = (function () {
    function processOrder() {
        this.orderTotal = 0;
        this.orderId = 0;
    }
    __decorate([
        core_1.Input(''),
        __metadata("design:type", Number)
    ], processOrder.prototype, "orderTotal", void 0);
    __decorate([
        core_1.Input(''),
        __metadata("design:type", Number)
    ], processOrder.prototype, "orderId", void 0);
    processOrder = __decorate([
        core_1.Component({
            selector: 'process-order',
            template: "<h2>Process Order Component</h2>\n\t\t\t <span>{{orderId}}:{{orderTotal}}</span>\t\n\t\t\t"
        }),
        __metadata("design:paramtypes", [])
    ], processOrder);
    return processOrder;
}());
exports.processOrder = processOrder;
//# sourceMappingURL=processorder.component.js.map