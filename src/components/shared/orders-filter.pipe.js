"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var _ = require("lodash");
var core_1 = require("@angular/core");
var ordersFilterPipe = (function () {
    function ordersFilterPipe() {
    }
    ordersFilterPipe.prototype.transform = function (array, query) {
        if (query) {
            console.log("In if of filter pipe", array);
            return _.filter(array, function (row) { return row.orderId.toString().indexOf(query) > -1; });
        }
        console.log("Array In else of filter pipe", array);
        return array;
    };
    ordersFilterPipe = __decorate([
        core_1.Pipe({
            name: "orderFilter"
        })
    ], ordersFilterPipe);
    return ordersFilterPipe;
}());
exports.ordersFilterPipe = ordersFilterPipe;
//# sourceMappingURL=orders-filter.pipe.js.map