import {Component, ElementRef} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service'
import { User } from '../../models/model-index'
import { Router } from  '@angular/router';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib/index';
// PouchDB.plugin(PouchFind)
import * as moment from "moment";
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';

@Component({
    selector: 'login-form',
    providers: [AuthenticationService],
    templateUrl: 'components/login/login.html',
    styleUrls: ['components/login/login.css']
})

export class Login {
  
  public usersDB = new PouchDB('users');
  public cloudantUsersDB = new PouchDB('https://aajeel.cloudant.com/users', {
    auth: {
      username: 'aajeel',
      password: 'aajeel@eyetea.co'
    }
  }); 
  public ibmUsersDB = new PouchDB('https://360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com/aajeelusers', {
    // auth: {
    //   username: 'arsie108@gmail.com',
    //   password: 'Admin123@'
    // }
    auth: {
      "username": "360527dd-869f-46ae-be21-c9b27c672b55-bluemix",
      "password": "591b1db46a44e67d7aeaf6323b36ee12b4484d03942c2f5dc942489d99417e9e",
      "host": "360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com",
      "port": 443,
      "url": "https://360527dd-869f-46ae-be21-c9b27c672b55-bluemix:591b1db46a44e67d7aeaf6323b36ee12b4484d03942c2f5dc942489d99417e9e@360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com"
    }
  });

  public errorMsg = '';
  users: any = [];
  authenticatedUser: any;
  loginForm: FormGroup;

  email: string = null;
  password: string = null;

  constructor(private _service: AuthenticationService, 
              private _router: Router, 
              private _formBuilder: FormBuilder) {
    // var self = this;
    // self.usersDB.bulkDocs([
    //   {_id: 'admin@admin.com',
    //    name: 'aajeel',
    //    email: 'admin@admin.com',
    //    password: 'admin9',
    //    role: 'admin'
    //   },
    //   {_id: 'operator@operator.com',
    //    name: 'arslan',
    //    email: 'operator@operator.com',
    //    password: 'operator9',
    //    role: 'operator'
    //  }
    // ]).then(function (result) {
    //   console.log(result);
    // }).catch(function (err) {
    //   console.log(err);
    // });
    this._buildLoginForm();
    this.replicateDatabase();
  }
  private _buildLoginForm(){
    this.loginForm = this._formBuilder.group({
      email: [null, Validators.required],
      password: [null, Validators.required]
    });
  }
  ngOnInit(){
    if (localStorage.getItem('user'))
        this._router.navigate(['home']);
  }
  syncDB(){

    var self = this;
    self.usersDB.sync(self.cloudantUsersDB,{
      live:true,
      retry: true
    }).on('change', function(change){
      console.log(change);
      self.usersDB.allDocs({
        include_docs: true,
        attachments: true
      }).then(function (result) {
        result.rows.map(function (row) { 
          self.users.push(row.doc); 
        });
        console.log('Users from user database:', self.users);
      }).catch(function (err) {
        console.log(err);
      });
    }).on('error', function(err){
      console.log(err);
    });
  }

  replicateDatabase(){
    var self = this
    var opts = { live: true, retry: true };
    self.usersDB.replicate.from(self.ibmUsersDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM CLOUDANT ONE-WAY USERS REPLICATION:--", info)
      self.usersDB.sync(self.ibmUsersDB, opts).on('change', function (info) {
        console.log("CHANGE EVENT FROM TWO-WAY USERS SYNC:--", info)
        if (info.direction === 'pull' || info.direction === 'push'){
          // self.()
        }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT FROM TWO-WAY SYNC USERS", err)
      }).on('active', function () {
        console.log("ACTIVE ACTIVE FROM TWO-WAY USERS SYNC!!")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR FROM SYNC USERS!!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR FROM REPLICATION CLOUDANT USERS !!", err)
    })
  }

  login(credentials) {
    var self = this;
    self.authenticatedUser = null;
    this._service.login(credentials.email, credentials.password).then(function (result){
      result.docs.map(function (row) { 
          self.authenticatedUser = row;
      });
      console.log("AUTHENTICATED USER: ", self.authenticatedUser);
      if (self.authenticatedUser != null){
        var sessionUser = {'name': self.authenticatedUser.name,
                           'email': self.authenticatedUser.email,
                           'role': self.authenticatedUser.role}
        // localStorage.setItem("authUser", this.sessionUser);
        localStorage.setItem("userRole", self.authenticatedUser.role);
        localStorage.setItem("user", self.authenticatedUser.email);
        self._router.navigate(['home']);
      }else{
        self.errorMsg = "Invalid Credentails";
        
        // return
      }
    }).catch(function (err){
      console.log(err);
    });
  }
}
