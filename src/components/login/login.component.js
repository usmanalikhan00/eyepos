"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var authentication_service_1 = require("../../services/authentication.service");
var router_1 = require("@angular/router");
var PouchDB = require("pouchdb/dist/pouchdb");
var forms_1 = require("@angular/forms");
var Login = (function () {
    function Login(_service, _router, _formBuilder) {
        this._service = _service;
        this._router = _router;
        this._formBuilder = _formBuilder;
        this.usersDB = new PouchDB('users');
        this.cloudantUsersDB = new PouchDB('https://aajeel.cloudant.com/users', {
            auth: {
                username: 'aajeel',
                password: 'eyeteaco'
            }
        });
        this.errorMsg = '';
        this.users = [];
        this.email = null;
        this.password = null;
        this._buildLoginForm();
    }
    Login.prototype._buildLoginForm = function () {
        this.loginForm = this._formBuilder.group({
            email: [null, forms_1.Validators.required],
            password: [null, forms_1.Validators.required]
        });
    };
    Login.prototype.ngOnInit = function () {
        var self = this;
        self.usersDB.sync(self.cloudantUsersDB, {
            live: true,
            retry: true
        }).on('change', function (change) {
            console.log(change);
        }).on('error', function (err) {
            console.log(err);
        });
        self.usersDB.allDocs({
            include_docs: true,
            attachments: true
        }).then(function (result) {
            result.rows.map(function (row) {
                self.users.push(row.doc);
            });
            console.log('Users from user database:', self.users);
        }).catch(function (err) {
            console.log(err);
        });
    };
    Login.prototype.login = function (credentials) {
        var self = this;
        self.authenticatedUser = null;
        this._service.login(credentials.email, credentials.password).then(function (result) {
            result.docs.map(function (row) {
                self.authenticatedUser = row;
            });
            console.log("AUTHENTICATED USER: ", self.authenticatedUser);
            if (self.authenticatedUser != null) {
                var sessionUser = { 'name': self.authenticatedUser.name,
                    'email': self.authenticatedUser.email,
                    'role': self.authenticatedUser.role
                };
                localStorage.setItem("userRole", self.authenticatedUser.role);
                localStorage.setItem("user", self.authenticatedUser.email);
                self._router.navigate(['home']);
            }
            else {
                self.errorMsg = "Invalid Credentails";
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    Login = __decorate([
        core_1.Component({
            selector: 'login-form',
            providers: [authentication_service_1.AuthenticationService],
            templateUrl: 'components/login/login.html',
            styleUrls: ['components/login/login.css']
        }),
        __metadata("design:paramtypes", [authentication_service_1.AuthenticationService, router_1.Router, forms_1.FormBuilder])
    ], Login);
    return Login;
}());
exports.Login = Login;
//# sourceMappingURL=login.component.js.map