"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var authentication_service_1 = require("../../services/authentication.service");
var products_service_1 = require("../../services/products.service");
var pos_service_1 = require("../../services/pos.service");
var common_1 = require("@angular/common");
var model_index_1 = require("../../models/model-index");
var forms_1 = require("@angular/forms");
var ng2_toasty_1 = require("ng2-toasty");
var PouchDB = require("pouchdb/dist/pouchdb");
var PouchFind = require("pouchdb-find/lib/index");
PouchDB.plugin(PouchFind);
var moment = require("moment");
var ngx_electron_1 = require("ngx-electron");
require('events').EventEmitter.prototype._maxListeners = 100;
var posComponent = (function () {
    function posComponent(_router, _service, _location, _formBuilder, toastyService, toastyConfig, _electronService) {
        this._router = _router;
        this._service = _service;
        this._location = _location;
        this._formBuilder = _formBuilder;
        this.toastyService = toastyService;
        this.toastyConfig = toastyConfig;
        this._electronService = _electronService;
        this.ordersDB = new PouchDB('orders');
        this.productsDB = new PouchDB('products');
        this.remoteDB = new PouchDB('http://127.0.0.1:5984/orders');
        this.cloudantDB = new PouchDB('https://aajeel.cloudant.com/orders', {
            auth: {
                username: 'aajeel',
                password: 'eyeteaco'
            }
        });
        this.cloudantProductDB = new PouchDB('https://aajeel.cloudant.com/products', {
            auth: {
                username: 'aajeel',
                password: 'eyeteaco'
            }
        });
        this.customersDB = new PouchDB('customers');
        this.cloudantCustomersDB = new PouchDB('https://aajeel.cloudant.com/customers', {
            auth: {
                username: 'aajeel',
                password: 'eyeteaco'
            }
        });
        this.filterQuery = "";
        this.rowsOnPage = 10;
        this.sortBy = "name";
        this.sortOrder = "asc";
        this.barCode = '';
        this.overAllDisc = 0;
        this.cartProducts = [];
        this.allProducts = [];
        this.cartTotal = 0;
        this.cashAlert = true;
        this.isPaymentValid = false;
        this.clickStockVal = true;
        this.checkStockValue = true;
        this.orderHistory = [];
        this.clickItemError = true;
        this.allCustomer = [];
        this.sattledCustomer = null;
        this.customerName = null;
        this.customerKey = null;
        this.customerPages = [];
        this.customerPageSize = 4;
        this._buildAddCustomerForm();
    }
    posComponent.prototype._buildAddCustomerForm = function () {
        this.addCustomerForm = this._formBuilder.group({
            name: [null, [forms_1.Validators.required]],
            phone: [null],
            email: [null],
            address: [null]
        });
    };
    posComponent.prototype.printInvoice = function () {
        console.log(this.orderId);
        this._electronService.ipcRenderer.send('loadPrintPage', this.orderId);
    };
    posComponent.prototype.ngOnInit = function () {
        var self = this;
        self.allProducts = [];
        self._service.getAllProducts().then(function (result) {
            result.rows.map(function (row) {
                self.allProducts.push(row.doc);
            });
            console.log("Call From ngOnInit(): ", self.allProducts);
        }).catch(function (err) {
            console.log(err);
        });
        var db = self.ordersDB.sync(self.cloudantDB, {
            live: true,
            retry: true
        });
        db.on('change', function (info) {
            console.log("Info from change sync:", info);
        }).on('error', function (err) {
            console.log("Info from Error sync:", err);
        });
        var pdb = self.productsDB.sync(self.cloudantProductDB, {
            live: true,
            retry: true
        });
        pdb.on('change', function (info) {
            console.log("Info from change sync:", info);
        }).on('error', function (err) {
            console.log("Info from Error sync:", err);
        });
        var cdb = self.customersDB.sync(self.cloudantCustomersDB, {
            live: true,
            retry: true
        });
        cdb.on('change', function (info) {
            console.log("Info from change sync:", info);
        }).on('error', function (err) {
            console.log("Info from Error sync:", err);
        });
    };
    posComponent.prototype.addOrderHistory = function () {
        var self = this;
        self.orderHistory = [];
        self._service.getAllOrders().then(function (result) {
            result.rows.map(function (row) {
                self.orderHistory.push(row.doc);
            });
            self.orderId = self.orderHistory.length;
            self.orderTimeStamp = new Date();
            var userAuth = localStorage.getItem('userRole');
            console.log("customer name: ", self.sattledCustomer);
            if (self.sattledCustomer != null) {
                self.customerName = self.sattledCustomer.name;
                self.customerKey = self.sattledCustomer._id;
            }
            var orderDoc = { '_id': new Date().toISOString(),
                'operatorName': userAuth,
                'customerName': self.customerName,
                'customerKey': self.customerKey,
                'orderItems': self.cartProducts,
                'orderId': self.orderHistory.length,
                'orderTotal': self.cartTotal,
                'orderDiscount': self.overAllDisc,
                'orderCash': self.changeAmount,
                'orderChange': (self.changeAmount - (self.cartTotal - self.overAllDisc)),
                'orderTimeStamp': self.orderTimeStamp,
                'orderMonth': moment(self.orderTimeStamp).format('MMMM'),
                'orderDay': moment(self.orderTimeStamp).format('D'),
                'orderYear': moment(self.orderTimeStamp).format('YYYY')
            };
            console.log("orderDoc after validate payment: ", orderDoc);
            var stockArrayToEdit = [];
            for (var i = 0; i < self.cartProducts.length; i++) {
                var docToPush = ({ '_id': self.cartProducts[i]._id,
                    '_rev': self.cartProducts[i]._rev,
                    'name': self.cartProducts[i].name,
                    'price': parseInt(self.cartProducts[i].price),
                    'stockval': parseInt(self.cartProducts[i].cartStockValue),
                    'productBarCode': self.cartProducts[i].productBarCode,
                });
                stockArrayToEdit.push(docToPush);
            }
            self.ordersDB.put(orderDoc).then(function (result) {
                console.log("Order to save:", result);
                self._service.updateStockValue(stockArrayToEdit).then(function (result) {
                    self.allProducts = [];
                    self._service.getAllProducts().then(function (result) {
                        result.rows.map(function (row) {
                            self.allProducts.push(row.doc);
                        });
                        var db = self.ordersDB.sync(self.cloudantDB, {
                            live: true,
                            retry: true
                        }).on('change', function (info) {
                            console.log("Info from change sync:", info);
                        }).on('error', function (err) {
                            console.log("Info from Error sync:", err);
                        });
                        var pdb = self.productsDB.sync(self.cloudantProductDB, {
                            live: true,
                            retry: true
                        }).on('change', function (info) {
                            console.log("Info from change sync:", info);
                        }).on('error', function (err) {
                            console.log("Info from Error sync:", err);
                        });
                    }).catch(function (err) {
                        console.log(err);
                    });
                }).catch(function (err) {
                    console.log(err);
                });
            }).catch(function (err) {
                console.log(err);
            });
        }).catch(function (err) {
            console.log(err);
        });
        this.closeModal();
    };
    posComponent.prototype.invoiceCloseModal = function () {
        this.changeAmount = null;
        this.overAllDisc = 0;
        this.isPaymentValid = false;
        this.cartTotal = 0;
        this.cartProducts = [];
    };
    posComponent.prototype.nextOrder = function () {
        this.changeAmount = null;
        this.isPaymentValid = false;
        this.overAllDisc = 0;
        this.cartTotal = 0;
        this.cartProducts = [];
        this.invoiceModalCloseBtn.nativeElement.click();
    };
    posComponent.prototype.overAllDiscCalc = function () {
        console.log("Overall Discount:", this.overAllDisc);
        if (this.overAllDisc < 0 || this.overAllDisc > this.cartTotal || this.overAllDisc == null)
            this.overAllDisc = 0;
    };
    posComponent.prototype.tenderAmount = function (e) {
        this.changeAmount = e;
        console.log("Cash Received:", this.changeAmount);
        if (this.changeAmount - (this.cartTotal - this.overAllDisc) >= 0) {
            this.isPaymentValid = true;
            this.cashAlert = true;
        }
        else if (this.changeAmount - (this.cartTotal - this.overAllDisc) < 0) {
            this.cashAlert = false;
            this.isPaymentValid = false;
        }
    };
    posComponent.prototype.changeQuantity = function (quantity, product) {
        product.cartStockValue = product.stockval - product.quantity;
        if (product.cartStockValue >= 0) {
            product.cartPrice = (product.price * product.quantity);
            this.calculateCartTotal(this.cartProducts);
            this.clickItemError = true;
            console.log("clickItemError", this.clickItemError);
        }
        else {
            product.quantity = 1;
            this.clickItemError = false;
            console.log("clickItemError", this.clickItemError);
        }
    };
    posComponent.prototype.changeDiscount = function (discount, product) {
        product.cartPrice = (product.price * product.quantity) - ((product.price * product.quantity) * (product.discount * 0.01));
        this.calculateCartTotal(this.cartProducts);
    };
    posComponent.prototype.process_barcode = function (event) {
        var self = this;
        if (event.charCode == 13) {
            console.log("Product BarCode:" + this.barCode);
            self._service.searchProduct(this.barCode).then(function (result) {
                console.log(result);
                result.docs.map(function (row) {
                    self.scannedProduct = row;
                });
                self.getProduct(self.scannedProduct);
                console.log("scanned product: ", self.scannedProduct);
                self.barCode = '';
                self.scannedProduct = '';
            }).catch(function (err) {
                console.log(err);
            });
        }
    };
    posComponent.prototype.getProduct = function (product) {
        console.log("In get Product");
        console.log("product", product);
        var self = this;
        var priceCalc;
        if (product.stockval > 0) {
            for (var i = 0; i < self.cartProducts.length; i++) {
                if (self.cartProducts[i]._id === product._id) {
                    self.cartProducts[i].quantity = self.cartProducts[i].quantity + 1;
                    Number(self.cartProducts[i].cartStockValue = Number(self.cartProducts[i].stockval) - Number(self.cartProducts[i].quantity));
                    if (self.cartProducts[i].cartStockValue >= 0) {
                        self.clickStockVal = true;
                        Number(self.cartProducts[i].cartPrice = Number(self.cartProducts[i].quantity) * Number(self.cartProducts[i].price));
                        self.calculateCartTotal(self.cartProducts);
                        return 0;
                    }
                    else {
                        self.clickStockVal = false;
                        self.cartProducts[i].quantity = 1;
                        Number(self.cartProducts[i].cartStockValue = Number(self.cartProducts[i].stockval) - Number(self.cartProducts[i].quantity));
                        Number(self.cartProducts[i].cartPrice = Number(self.cartProducts[i].quantity) * Number(self.cartProducts[i].price));
                        self.calculateCartTotal(self.cartProducts);
                        return 0;
                    }
                }
            }
            var docToPush = { '_id': product._id,
                '_rev': product._rev,
                'name': product.name,
                'price': product.price,
                'productBarCode': product.productBarCode,
                'discount': 0,
                'quantity': 1,
                'stockval': product.stockval,
                'cartStockValue': product.stockval - 1,
                'cartPrice': product.price };
            self.cartProducts.push(docToPush);
            self.calculateCartTotal(self.cartProducts);
        }
    };
    posComponent.prototype.calculateCartTotal = function (cartProducts) {
        var cartTotal = 0;
        for (var i = 0; i < cartProducts.length; i++) {
            Number(cartTotal = Number(cartTotal) + Number(cartProducts[i].cartPrice));
        }
        this.cartTotal = cartTotal;
    };
    posComponent.prototype.removeProduct = function (cartProducts, product, i) {
        console.log(cartProducts);
        console.log(product);
        console.log(i);
        cartProducts.splice(i, 1);
        this.cartProducts = cartProducts;
        this.calculateCartTotal(this.cartProducts);
    };
    posComponent.prototype.clearCart = function () {
        this.cartProducts = [];
        this.cartTotal = 0;
    };
    posComponent.prototype.closeModal = function () {
        this.closeBtn.nativeElement.click();
    };
    posComponent.prototype.goBack = function () {
        if (localStorage.getItem("userRole") === "operator") {
            localStorage.removeItem("user");
            this._router.navigate(['login']);
        }
        else {
            this._router.navigate(['home']);
        }
    };
    posComponent.prototype.refundOrder = function () {
        this._router.navigate(['invoice']);
    };
    posComponent.prototype.getCustomers = function () {
        var self = this;
        self.allCustomer = [];
        self.customerPages = [];
        console.log("All Customers Array Lenght: ", self.allCustomer.length);
        self._service.getAllCustomers().then(function (result) {
            console.log("RESULT FROM GETTING ALL THE CUSTOMERS: ", result);
            result.rows.map(function (row) {
                self.allCustomer.push(row.doc);
            });
            for (var i = 0; i <= Math.floor(self.allCustomer.length / self.customerPageSize); i++) {
                self.customerPages.push(i + 1);
            }
            self.getCustomersPage(1);
            console.log("CUSTOMER PAGE ARRAY: ", self.customerPages);
            console.log("CUSTOMERS AFTER ROW MAPPING: ", self.allCustomer);
        }).catch(function (err) {
            console.log(err);
        });
    };
    posComponent.prototype.getCustomersPage = function (pageNumb) {
        var self = this;
        self.currentPage = pageNumb;
        var limit = self.customerPageSize;
        if ((self.currentPage > self.customerPages.length) || (self.currentPage <= 0)) {
            if ((self.currentPage <= 0)) {
                self.currentPage = self.customerPages.length;
            }
            else {
                self.currentPage = 1;
            }
        }
        var skip = limit * (self.currentPage - 1);
        self.allCustomer = [];
        self._service.getCustomerPage(limit, skip).then(function (result) {
            console.log("RESULT WITH LIMIT QUERY: ", result);
            result.rows.map(function (row) {
                self.allCustomer.push(row.doc);
            });
            console.log("ALL CUSTOMERS LIMIT QUERY: ", result);
        }).catch(function (err) {
            console.log(err);
        });
    };
    posComponent.prototype.addCustomer = function (addCustomerValues) {
        var self = this;
        var customer = new model_index_1.Customer(new Date().toISOString(), addCustomerValues.name, addCustomerValues.phone, addCustomerValues.email, addCustomerValues.address);
        console.log("customer to add in database: ", customer);
        self._service.addCustomer(customer).then(function (result) {
            console.log("SUCCESSFUL RESULT AFTER ADDING CUSTOMER: ", result);
            self._buildAddCustomerForm();
            self.getCustomers();
        }).catch(function (err) {
            console.log(err);
        });
    };
    posComponent.prototype.select = function (index, customer, $event) {
        var self = this;
        console.log("this.selectedIndex: ", self.selectedIndex);
        console.log("SELECTED CUSTOMER: ", customer);
        self.sattledCustomer = customer;
        console.log("SATTLED CUSTOMER: ", self.sattledCustomer);
        var target = $event.srcElement.attributes.class;
        console.log("SELECTED CUSTOMER CLASS 2: ", target);
        var randomNumb = self.getRandomInt(1, 100000000);
        console.log("RANDOM NUMBER GENERATED: ", randomNumb);
        if (self.selectedIndex == index) {
            self.selectedIndex = null;
            self.sattledCustomer = null;
        }
        else {
            self.selectedIndex = index;
        }
    };
    posComponent.prototype.getRandomInt = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    __decorate([
        core_1.ViewChild('closeBtn'),
        __metadata("design:type", core_1.ElementRef)
    ], posComponent.prototype, "closeBtn", void 0);
    __decorate([
        core_1.ViewChild('invoiceModalCloseBtn'),
        __metadata("design:type", core_1.ElementRef)
    ], posComponent.prototype, "invoiceModalCloseBtn", void 0);
    __decorate([
        core_1.ViewChild('autoFocusInput'),
        __metadata("design:type", core_1.ElementRef)
    ], posComponent.prototype, "autoFocusInput", void 0);
    posComponent = __decorate([
        core_1.Component({
            selector: 'pos-content',
            templateUrl: 'components/pos/pos.html',
            styleUrls: ['components/pos/pos.css'],
            providers: [authentication_service_1.AuthenticationService, products_service_1.ProductsService, pos_service_1.PosService]
        }),
        __metadata("design:paramtypes", [router_1.Router,
            pos_service_1.PosService,
            common_1.Location,
            forms_1.FormBuilder,
            ng2_toasty_1.ToastyService,
            ng2_toasty_1.ToastyConfig,
            ngx_electron_1.ElectronService])
    ], posComponent);
    return posComponent;
}());
exports.posComponent = posComponent;
//# sourceMappingURL=pos.component.js.map