import {Component, 
        Directive, 
        HostListener, 
        Renderer, 
        ViewChild, 
        ElementRef, 
        NgZone, 
        ChangeDetectorRef, 
        Input, 
        ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';
import {ProductsService} from '../../services/products.service';
import {PosService} from '../../services/pos.service';
import { Location } from '@angular/common';
import { User, Product, Customer } from '../../models/model-index';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib/index'
PouchDB.plugin(PouchFind)
import * as moment from "moment";
import {ElectronService} from 'ngx-electron';
require('events').EventEmitter.prototype._maxListeners = 100;
import * as _ from 'lodash'

export enum KEY_CODE {
  RIGHT_ARROW = 39,
  LEFT_ARROW = 37
}

@Component({
  selector: 'pos-content',
  templateUrl: 'components/pos/pos.html',
  styleUrls: ['components/pos/pos.css'],
  providers: [AuthenticationService, ProductsService, PosService]
  // host: {
  //       '(document:keypress)': 'handleKeyboardEvents($event)'
  //   }
})

export class posComponent{

  @ViewChild('closeBtn') closeBtn: ElementRef;
  @ViewChild('invoiceModalCloseBtn') invoiceModalCloseBtn: ElementRef;
  @ViewChild('autoFocusInput') autoFocusInput: ElementRef;
  // @ViewChild('alert') alert: ElementRef;

  public ordersDB = new PouchDB('orders');
  public productsDB = new PouchDB('products');
  // public productsDB = new PouchDB('products');
  public remoteDB = new PouchDB('http://127.0.0.1:5984/orders');
  // public cloudantDB = new PouchDB('https://aajeel.cloudant.com/orders', {
  //   auth: {
  //     username: 'aajeel',
  //     password: 'aajeel@eyetea.co'
  //   }
  // });
  public ibmOrdersDB = new PouchDB('https://360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com/aajeelorders', {
    auth: {
      "username": "360527dd-869f-46ae-be21-c9b27c672b55-bluemix",
      "password": "591b1db46a44e67d7aeaf6323b36ee12b4484d03942c2f5dc942489d99417e9e",
      "host": "360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com",
      "port": 443,
      "url": "https://360527dd-869f-46ae-be21-c9b27c672b55-bluemix:591b1db46a44e67d7aeaf6323b36ee12b4484d03942c2f5dc942489d99417e9e@360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com"
    }
    // auth: {
    //   username: 'arsie108@gmail.com',
    //   password: 'Admin123@'
    // }
  });  
  // public cloudantProductDB = new PouchDB('https://aajeel.cloudant.com/products', {
  //   auth: {
  //     username: 'aajeel',
  //     password: 'aajeel@eyetea.co'
  //   }
  // });
  public cloudantProductsDB = new PouchDB('https://360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com/aajeelproducts', {
    auth: {
      "username": "360527dd-869f-46ae-be21-c9b27c672b55-bluemix",
      "password": "591b1db46a44e67d7aeaf6323b36ee12b4484d03942c2f5dc942489d99417e9e",
      "host": "360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com",
      "port": 443,
      "url": "https://360527dd-869f-46ae-be21-c9b27c672b55-bluemix:591b1db46a44e67d7aeaf6323b36ee12b4484d03942c2f5dc942489d99417e9e@360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com"
    }
    // auth: {
    //   username: 'arsie108@gmail.com',
    //   password: 'Admin123@'
    // }
  });

  public customersDB = new PouchDB('customers');
  public cloudantCustomersDB = new PouchDB('https://aajeel.cloudant.com/customers', {
    auth: {
      username: 'aajeel',
      password: 'aajeel@eyetea.co'
    }
  });
  public ibmCustomersDB = new PouchDB('https://360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com/aajeelcustomers', {
    auth: {
      "username": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix",
      "password": "e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad",
      "host": "897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com",
      "port": 443,
      "url": "https://897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix:e8c22632517b608b207c123cbc38251aba8965506def60ed64fa6ff88514e4ad@897d8fbd-1150-44cb-a3cf-28bbbbc88993-bluemix.cloudant.com"
    }
    // auth: {
    //   username: 'deramossawillowevicatara',
    //   password: 'b95743aad28f1741e5dab1ca9172436a0e8a4aac'
    // }
    // auth: {
    //   username: 'arsie108@gmail.com',
    //   password: 'Admin123@'
    // }
    
  });

  public data;
  public filterQuery = "";
  public rowsOnPage = 10;
  public sortBy = "name";
  public sortOrder = "asc";
  barCode = '';

  addCustomerForm: FormGroup;
  cashPaidAmount: any;
  key: any;
  overAllDisc: any = null;
  cartProducts = [];
  allProducts: any = [];
  cartTotal=0;
  changeAmount: any = null;
  cashAlert = true;
  isPaymentValid = false;
  clickStockVal = true;
  checkStockValue = true;
  orderHistory: any = [];
  barCodeProduct: any;
  orderId: any;
  orderTimeStamp: any;
  clickItemError: boolean = true;
  discountedCartTotal: any;
  scannedProduct: any;
  allCustomer: any = [];
  sattledCustomer: any = null;
  customerName: any = null;
  customerKey: any = null;
  customerPages: any = [];
  customerPageSize = 4;
  currentPage: any;

  productType: any = null
  productWeight: any = null
  calcWeight: any = null
  productBarcode: any = null
  weightDiff: any =  null
  selectedTab: any =  null
  cash: any =  null


  @ViewChild('barcode') barcode; 
  @ViewChild('cashoutbtn') cashoutbtn; 

  constructor(private _router: Router,
              private _service: PosService,
              private _location: Location,
              private _formBuilder: FormBuilder,
              private _renderer: Renderer,
              private toastyService: ToastyService,
              private toastyConfig: ToastyConfig,
              private _electronService: ElectronService){
      this._buildAddCustomerForm();
      this.replicateDatabase()
  }
  private _buildAddCustomerForm() {
    this.addCustomerForm = this._formBuilder.group({
      name: [null, [Validators.required]],
      phone: [null],
      email: [null],
      address: [null]
    });
  }

  // dueAmount(val : FormControl) :any {
  //     // console.log('match', this.processOrderForm.value);
  //   // console.log('formValidator ' + val.value);
  //   //   console.log('match', this.processOrderForm.controls['cashPaid'].value);
  //   //   this.cashPaidAmount = val.value;
  //   //   console.log('formValidator222 ' + this.cashPaidAmount);
  //   // if(val.value.match(/.*[^0-9].*/)){
  //   //     return {inval : true};
  //   // }
  //     // return {inval : false};

  // }

  printInvoice(){
    // this._router.navigate(['/transactions', this.orderId])
    this._electronService.ipcRenderer.send('loadPrintPage', this.orderId);
  }

  setBarCodeFocus(){
    const element = this._renderer.selectRootElement('#inlineFormInputGroup');
    setTimeout(() => element.focus(), 0);
    // this.barCode = ''
  }
  setCashRecFocus(){
    const element = this._renderer.selectRootElement('#amountpaid');
    setTimeout(() => element.focus(), 0);
    // console.log('VALUE OF BARCODE INPUT:---', this.barcode.nativeElement.value);
    // this.barCode = ''
  }

  barCodeChanged($event){
    console.log()
  }


  replicateDatabase(){
    var self = this
    var opts = { live: true, retry: true };
    self.customersDB.replicate.from(self.ibmCustomersDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM CLOUDANT ONE-WAY CUSTOMERS REPLICATION:--", info)
      self.customersDB.sync(self.ibmCustomersDB, opts).on('change', function (info) {
        console.log("CHANGE EVENT FROM TWO-WAY CUSTOMERS SYNC:--", info)
        if (info.direction === 'pull' || info.direction === 'push'){
          // self.()
        }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT FROM TWO-WAY SYNC CUSTOMERS", err)
      }).on('active', function () {
        console.log("ACTIVE ACTIVE FROM TWO-WAY CUSTOMERS SYNC!!")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR FROM SYNC CUSTOMERS!!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR FROM REPLICATION CLOUDANT CUSTOMERS !!", err)
    })

    self.productsDB.replicate.from(self.cloudantProductsDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM CLOUDANT ONE-WAY PRODUCTS REPLICATION:--", info)
      self.customersDB.sync(self.ibmCustomersDB, opts).on('change', function (info) {
        console.log("CHANGE EVENT FROM TWO-WAY PRODUCTS SYNC:--", info)
        if (info.direction === 'pull' || info.direction === 'push'){
          // self.()
        }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT FROM TWO-WAY SYNC PRODUCTS", err)
      }).on('active', function () {
        console.log("ACTIVE ACTIVE FROM TWO-WAY PRODUCTS SYNC!!")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR FROM SYNC PRODUCTS!!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR FROM REPLICATION CLOUDANT PRODUCTS !!", err)
    })


  }
  

  // hookOrder(){

  // }

  handleKeyboardEvents(event: KeyboardEvent) {
    var self = this;
    self.key = event.key;
    console.log("EVENT KEY PRESSED:-----", event)
    if (self.key === 'F2'){
      self.setBarCodeFocus()
    }else if (self.key === 'Shift'){
      // this._renderer.selectRootElement('#inlineFormInputGroup').innerHtml = ''
      self.setCashRecFocus()
      // self.barCode = ''
    }else if (self.key === 'Control'){
      self.cashOut()
    }
  }
  
  ngOnInit(){
    var self = this;
    self.allProducts = [];
    self.orderHistory = [];
    self.setBarCodeFocus()
    // self.replicateDatabase()
    self._service.getAllProducts().then(function (result) {
      result.rows.map(function (row) {
        self.allProducts.push(row.doc);
      });

    }).catch(function (err) {
      console.log(err);
    });
  }

    // Chnage calculations on received amount from customer
  tenderAmount(e){
    var self = this
    self.changeAmount = e;
    self.cash = _.cloneDeep(e);
    console.log("Cash Received:" , self.changeAmount, self.cash);
    if (self.changeAmount - (self.cartTotal - self.overAllDisc) >= 0 ){
      this.isPaymentValid = true;
      self.cashAlert = true;
    }else if (self.changeAmount - (self.cartTotal - self.overAllDisc) < 0 ){
      self.cashAlert = false;
      self.isPaymentValid = false;
    }
     // = self.changeAmount
  }

  // adding order history to Orders table
  addOrderHistory(){
    var self = this;
    self.orderTimeStamp = new Date();
    var userAuth = localStorage.getItem('userRole');
    // console.log("customer name: ", self.sattledCustomer);
    // console.log("Current date time stamp: ", moment(self.orderTimeStamp).format('MMMM'));
    if (self.sattledCustomer != null){
      self.customerName = self.sattledCustomer.name;
      self.customerKey = self.sattledCustomer._id;
    }

    self._service.getAllOrders().then(function (result) {
      // result.rows.map(function (row) {
      //   self.orderHistory.push(row.doc);
      // });
      self.orderId = result.rows[0].doc.orderId+1;
      console.log("__________+++++++++++++Order History Array: ", result, self.orderHistory);
      console.log("Order History Array ORDER ID___________++++++++++++++: ", self.orderId);
      // console.log("Call From ngOnInit(): ", self.allProducts);
      console.log("CHANGE AMOUNT RECEIVED: ", self.changeAmount, self.cash);
      var orderDoc = {'_id': new Date().toISOString(),
                      'operatorName': userAuth,
                      'customerName': self.customerName,
                      'customerKey': self.customerKey,
                      'orderItems': self.cartProducts,
                      'orderId': self.orderId,
                      'orderTotal': self.cartTotal,
                      'orderDiscount': self.overAllDisc,
                      'orderCash': self.changeAmount,
                      'orderChange': (self.changeAmount - (self.cartTotal - self.overAllDisc)),
                      'orderTimeStamp': self.orderTimeStamp,
                      'orderMonth': moment(self.orderTimeStamp).format('MMMM'),
                      'orderDay': moment(self.orderTimeStamp).format('D'),
                      'orderYear': moment(self.orderTimeStamp).format('YYYY')
                      };
        console.log("orderDoc after validate payment: ", orderDoc);
          // console.log("orderDoc after validate payment: ", orderDoc);
        var stockArrayToEdit = [];
        for (let i=0; i<self.cartProducts.length; i++){
          // console.log("item Array object: ", self.cartProducts[i]);
          if (self.cartProducts[i].type === 'singleunit'){

            var docToPush = ({'_id': self.cartProducts[i]._id,
                             '_rev': self.cartProducts[i]._rev,
                             'name': self.cartProducts[i].name,
                             'price': parseInt(self.cartProducts[i].price),
                             'stockval': parseInt(self.cartProducts[i].cartStockValue),
                             'productBarCode':self.cartProducts[i].productBarCode,
                             'type':self.cartProducts[i].type,
                             'weight':"none"
                            });
            // console.log("docToPush: ", docToPush);
            stockArrayToEdit.push(docToPush);
            // break
          }
          if (self.cartProducts[i].type === 'weighted'){
            self.weightDiff = self.cartProducts[i].weight - self.cartProducts[i].cartStockWeight
            var doc = ({'_id': self.cartProducts[i]._id,
                       '_rev': self.cartProducts[i]._rev,
                       'name': self.cartProducts[i].name,
                       'price': parseInt(self.cartProducts[i].price),
                       'stockval': self.cartProducts[i].stockval,
                       'productBarCode':self.cartProducts[i].productBarCode,
                       'type':self.cartProducts[i].type,
                       'weight':self.weightDiff
                      });
            // console.log("docToPush: ", docToPush);
            stockArrayToEdit.push(doc);
            // break
          }
          console.log("stockArrayToEdit: ", stockArrayToEdit);
        }
        self.ordersDB.put(orderDoc).then(function(result) {
          // console.log("Order to save:", result);
          self._service.updateStockValue(stockArrayToEdit).then(function(result){
              // console.log("after update stock value", result);
            self.allProducts = [];
            self._service.getAllProducts().then(function (result) {
              result.rows.map(function (row) {
                self.allProducts.push(row.doc);
              });
              self.nextOrder()
              self.printInvoice()
            }).catch(function (err) {
              console.log(err);
            });
          }).catch(function(err){
            console.log(err);
          });
        }).catch(function(err){
          console.log(err);
        });
    }).catch(function (err) {
      console.log(err);
    });
  }
  invoiceCloseModal(){
    this.changeAmount=null;
    this.overAllDisc = 0;
    this.isPaymentValid = false;
    this.cartTotal = 0;
    this.cartProducts = [];
  }
  // Clearing order list for next order
  nextOrder(){
    this.changeAmount=null;
    this.isPaymentValid = false;
    this.overAllDisc = null;
    this.cartTotal = 0;
    this.cartProducts = [];
    // this.orderId++;
    // this.changeAmount = null
    this.isPaymentValid = false
    // this.invoiceModalCloseBtn.nativeElement.click();
  }

  overAllDiscCalc(){
    // console.log("Overall Discount:" , this.overAllDisc);
    if (this.overAllDisc < 0 || this.overAllDisc > this.cartTotal || this.overAllDisc == null)
       this.overAllDisc = null;
  }



  // quantity change for a product in order list
  changeQuantity(quantity, product){
    product.cartStockValue = product.stockval - product.quantity;
    product.cartPrice = (product.price * product.quantity);
    this.calculateCartTotal(this.cartProducts);
    if (quantity<=0){
      product.quantity = 1;
    }
    this.tenderAmount(this.changeAmount)
  }

  // discount change for a product in order list
  changeDiscount(discount, product){
    product.cartPrice = (product.price * product.quantity)-((product.price * product.quantity) * (product.discount * 0.01));
    this.calculateCartTotal(this.cartProducts);
  }
  process_barcode(event){
    var self = this;
    // console.log("BARCODE FROM INPUT:-----", event)
    if (event.charCode == 13){
      console.log("Product BarCode:----", self.barCode, self.barCode.length);
      for (let i=0; i<self.barCode.length; i++){
      	if (i<=1){
      		if (self.productType === null){
      			self.productType = self.barCode[i]
      		}else{
      			self.productType = self.productType+self.barCode[i]
      		}
      	}
      	if (i>1 && i<=6){
      		if (self.productBarcode === null){
      			self.productBarcode = self.barCode[i]
      		}else{
      			self.productBarcode = self.productBarcode+self.barCode[i]
      		}
      	}
      	if (i>=7 && i<=11){
      		if (self.productWeight === null){
      			self.productWeight = self.barCode[i]
      		}else{
      			self.productWeight = self.productWeight+self.barCode[i]
      		}
      	}
        var flag = parseInt(self.productWeight)
        self.calcWeight = flag/1000 
        // this.productWeight = flag/1000
      }
    	console.log("BARCODE INFORMATION AFTER PROCESSING:-----\n", 
                  "1:---", self.productType, 
                  "\n2:----", self.productBarcode, 
                  "\n3:----", self.productWeight, typeof(self.productWeight), self.calcWeight, typeof(flag))
      if (self.productType === '98'){

        self._service.searchProduct(self.productBarcode).then(function(result){
          // console.log(result);
          result.docs.map(function (row) {
              row.scanned = true
              self.scannedProduct = row;
          });
          console.log("scanned product: ",self.scannedProduct);
          self.barCode=''
          self.productBarcode = null
          self.productWeight = null
          self.productType = null
          self.getProduct(self.scannedProduct);
          self.scannedProduct = ''
        }).catch(function(err){
          console.log(err);
        })
      }else{
        self._service.searchProduct(self.barCode).then(function(result){
          // console.log(result);
          result.docs.map(function (row) {
              row.scanned = false
              self.scannedProduct = row;
          });
          console.log("scanned product: ",self.scannedProduct);
          self.barCode=''
          self.productBarcode = null
          self.productWeight = null
          self.productType = null
          self.getProduct(self.scannedProduct);
          self.scannedProduct = ''
          self.setBarCodeFocus()
        }).catch(function(err){
          console.log(err);
        })


      }
    }
  }
  // products to push in order list
  getProduct(product){
    // console.log("In get Product");
    // console.log("product", product);
    if (!product.scanned){
      product.scanned = false
    }
    var self = this;
    var priceCalc:any;
    var docToPush: any;
    self.selectedTab = product._id
    // if (product.stockval > 0){
      for(let i =0; i<self.cartProducts.length; i++){
        if(self.cartProducts[i]._id === product._id){
          if (product.type === 'singleunit'){

            self.cartProducts[i].quantity = self.cartProducts[i].quantity + 1;
            Number(self.cartProducts[i].cartStockValue = Number(self.cartProducts[i].stockval) - Number(self.cartProducts[i].quantity));
            Number(self.cartProducts[i].cartPrice = Number(self.cartProducts[i].quantity) * Number(self.cartProducts[i].price));
            self.calculateCartTotal(self.cartProducts);
            self.tenderAmount(this.changeAmount)
            // console.log("CART PRODUCTS:---", self.cartProducts)
            return 0;
          }else{
            if (product.scanned){
              self.cartProducts[i].quantity = self.cartProducts[i].quantity + 1;
              Number(self.cartProducts[i].cartStockWeight = Number(self.cartProducts[i].cartStockWeight) + Number(self.calcWeight*1000));
              Number(self.cartProducts[i].calcweight = Number(self.cartProducts[i].weight) - Number(self.cartProducts[i].cartStockWeight));
              Number(self.cartProducts[i].cartPrice = Number(self.cartProducts[i].cartStockWeight/1000) * Number(self.cartProducts[i].price));
              self.calculateCartTotal(self.cartProducts);
              self.tenderAmount(this.changeAmount)
              // console.log("CART PRODUCTS:---", self.cartProducts)
              return 0;
            }else{

              self.cartProducts[i].quantity = self.cartProducts[i].quantity + 1;
              Number(self.cartProducts[i].cartStockWeight = Number(self.cartProducts[i].cartStockWeight) + Number(1*1000));
              Number(self.cartProducts[i].calcweight = Number(self.cartProducts[i].weight) - Number(self.cartProducts[i].cartStockWeight));
              Number(self.cartProducts[i].cartPrice = Number(self.cartProducts[i].cartStockWeight/1000) * Number(self.cartProducts[i].price));
              self.calculateCartTotal(self.cartProducts);
              self.tenderAmount(this.changeAmount)
              // console.log("CART PRODUCTS:---", self.cartProducts)
              return 0;

            }

          }

        }
      }
      if (product.type === 'singleunit'){

        docToPush = {'_id':product._id,
                          '_rev': product._rev,
                          'name':product.name,
                          'price':product.price,
                          'productBarCode':product.productBarCode,
                          'discount': 0,
                          'quantity': 1,
                          'scanned':product.scanned,
                          'weight':product.weight,
                          'type':product.type,
                          'stockval':product.stockval,
                          'cartStockValue':product.stockval-1,
                          'cartStockWeight':null,
                          'cartPrice':product.price}
        self.cartProducts.push(docToPush);
        self.calculateCartTotal(self.cartProducts);
        self.tenderAmount(this.changeAmount)
        // console.log("CART PRODUCTS:---", self.cartProducts)
      }else{
        if (product.scanned){

          docToPush = {'_id':product._id,
                      '_rev': product._rev,
                      'name':product.name,
                      'price':product.price,
                      'productBarCode':product.productBarCode,
                      'discount': 0,
                      'quantity': 1,
                      'scanned':product.scanned,
                      'weight':product.weight,
                      'type':product.type,
                      'stockval':product.stockval,
                      'cartStockValue':null,
                      'cartStockWeight':self.calcWeight*1000,
                      'calcweight':product.weight - self.calcWeight*1000,
                      'cartPrice':product.price*(self.calcWeight*1000/1000)}
          self.cartProducts.push(docToPush);
          self.calculateCartTotal(self.cartProducts);
          self.tenderAmount(this.changeAmount)
          // console.log("CART PRODUCTS:---", self.cartProducts)
        }else{
          docToPush = {'_id':product._id,
                      '_rev': product._rev,
                      'name':product.name,
                      'price':product.price,
                      'productBarCode':product.productBarCode,
                      'discount': 0,
                      'quantity': 1,
                      'scanned':product.scanned,
                      'weight':product.weight,
                      'type':product.type,
                      'stockval':product.stockval,
                      'cartStockValue':null,
                      'cartStockWeight':1*1000,
                      'calcweight':product.weight - 1*1000,
                      'cartPrice':product.price*(1*1000/1000)}
          self.cartProducts.push(docToPush);
          self.calculateCartTotal(self.cartProducts);
          self.tenderAmount(this.changeAmount)
          // console.log("CART PRODUCTS:---", self.cartProducts)
        }
      }
    // }
  }


  cashOut(){
    // console.log("CASH OUT CLICKED:----")
    if (this.cartProducts.length != 0 && this.isPaymentValid)
      this.addOrderHistory()
    else
      console.log("CLICK WITH NULL ARRAY")
  }

  // calculating order list total
  calculateCartTotal(cartProducts){
    var cartTotal = 0;
    for(let i =0; i<cartProducts.length; i++){
        Number(cartTotal = Number(cartTotal) + Number(cartProducts[i].cartPrice));
    }
    this.cartTotal = cartTotal;
  }
  removeProduct(cartProducts, product, i){
    // console.log(cartProducts);
    // console.log(product);
    // console.log(i);
    cartProducts.splice(i, 1);
    this.cartProducts = cartProducts;
    this.calculateCartTotal(this.cartProducts);
  }
  //  clear the cart array
  clearCart(){
    this.cartProducts = [];
    this.cartTotal = 0;
    this.overAllDisc = null
    this.changeAmount = null
    this.isPaymentValid = false
  }

  closeModal() {
    this.closeBtn.nativeElement.click();
  }

  goBack(){
    if (localStorage.getItem("userRole")==="operator"){
      localStorage.removeItem("user");
      this._router.navigate(['login']);
    }else{
      this._router.navigate(['home']);
    }
  }
  refundOrder(){
    this._router.navigate(['invoice']);
  }
  getCustomers(){
    var self = this;
    self.allCustomer = [];
    self.customerPages = [];
    console.log("All Customers Array Lenght: ", self.allCustomer.length);
    self._service.getAllCustomers().then(function(result){
      // console.log("RESULT FROM GETTING ALL THE CUSTOMERS: ", result);
      result.rows.map(function (row) {
        self.allCustomer.push(row.doc);
      });
      for (let i=0; i<=Math.floor(self.allCustomer.length/self.customerPageSize); i++){
        self.customerPages.push(i+1);
      }
      self.getCustomersPage(1);
      // self.cusrrentPage = 1;
      // console.log("CUSTOMER PAGE ARRAY: ", self.customerPages);
      // console.log("CUSTOMERS AFTER ROW MAPPING: ", self.allCustomer);
    }).catch(function(err){
      console.log(err);
    })
  }
  getCustomersPage(pageNumb){
    var self = this;
    self.currentPage = pageNumb;
    var limit = self.customerPageSize;
    if ((self.currentPage > self.customerPages.length) || (self.currentPage <= 0)){
      
      if((self.currentPage <= 0)){
        self.currentPage = self.customerPages.length;
      }else{
        self.currentPage = 1;
      }
    }
    var skip = limit * (self.currentPage - 1);
    self.allCustomer = [];
    self._service.getCustomerPage(limit, skip).then(function(result){
      // console.log("RESULT WITH LIMIT QUERY: ", result);
      result.rows.map(function (row) {
        self.allCustomer.push(row.doc);
      });
      // console.log("ALL CUSTOMERS LIMIT QUERY: ", result);

    }).catch(function(err){
      console.log(err);
    });

  }
  addCustomer(addCustomerValues){
    var self = this;
    var customer = new Customer(new Date().toISOString(), addCustomerValues.name, addCustomerValues.phone, addCustomerValues.email, addCustomerValues.address);
    // console.log("customer to add in database: ", customer);
    self._service.addCustomer(customer).then(function(result){
      // console.log("SUCCESSFUL RESULT AFTER ADDING CUSTOMER: ", result);
      self._buildAddCustomerForm();
      self.getCustomers();
    }).catch(function(err){
      console.log(err);
    });
  }
  selectedIndex: string;

  select(index: string, customer: Customer, $event) {
    var self = this;
    // console.log("this.selectedIndex: ", self.selectedIndex);
    // console.log("SELECTED CUSTOMER: ", customer);
    self.sattledCustomer = customer;
    // console.log("SATTLED CUSTOMER: ", self.sattledCustomer);
    // var target = $event.srcElement.attributes.class || $event.currentTarget.class;
    // console.log("SELECTED CUSTOMER CLASS: ", target.attributes.class);
    var target: String = $event.srcElement.attributes.class;
    // console.log("SELECTED CUSTOMER CLASS 2: ", target);
    // if (($event.srcElement.attributes.class).indexOf('active')){

    // }
    var randomNumb = self.getRandomInt(1, 100000000);
    // console.log("RANDOM NUMBER GENERATED: ", randomNumb);
    if(self.selectedIndex == index){
      self.selectedIndex = null;
      self.sattledCustomer = null;
    }else{
      self.selectedIndex = index;
    }
    
  }
  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  // @HostListener('window:keyup', ['$event'])
  //   keyEvent(event: KeyboardEvent) {
  //     var self = this
  //     // console.log(event);
  //     self.key = event.key;
  //     // console.log("EVENT KEY PRESSED:-----", event)
  //     if (self.key === 'F2'){
  //       self.setBarCodeFocus()
  //     }else if (self.key === 'Shift'){
  //       // this._renderer.selectRootElement('#inlineFormInputGroup').innerHtml = ''
  //       self.setCashRecFocus()
  //       // self.barCode = ''
  //     }else if (self.key === 'Control'){
  //       self.cashOut()
  //     }

  //     // if (event.keyCode === KEY_CODE.RIGHT_ARROW) {
  //     //   // this.increment();
  //     //   console.log("RIGHT ARROW KEY:---", event.keyCode)
  //     // }

  //     // if (event.keyCode === KEY_CODE.LEFT_ARROW) {
  //     //   console.log("LEFT ARROW KEY:---", event.keyCode)
  //     //   // this.decrement();
  //     // }
  //   }
}
