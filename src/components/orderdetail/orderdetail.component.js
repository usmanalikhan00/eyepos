"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var orderdetail_service_1 = require("../../services/orderdetail.service");
var PouchDB = require("pouchdb/dist/pouchdb");
var PouchFind = require("pouchdb-find/lib/index");
PouchDB.plugin(PouchFind);
var ngx_electron_1 = require("ngx-electron");
var orderDetailComponent = (function () {
    function orderDetailComponent(_router, _service, _electronService) {
        this._router = _router;
        this._service = _service;
        this._electronService = _electronService;
    }
    orderDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        this.sub = this._router.params.subscribe(function (params) {
            _this.id = +params['id'];
            self._service.findOrder(_this.id).then(function (result) {
                result.docs.map(function (row) {
                    self.orderToShow = row;
                });
                self._electronService.ipcRenderer.send('Print');
            }).catch(function (err) {
                console.log(err);
            });
        });
    };
    orderDetailComponent.prototype.findOrder = function (id) {
        var self = this;
    };
    orderDetailComponent = __decorate([
        core_1.Component({
            selector: 'order-detail',
            templateUrl: 'components/orderdetail/orderdetail.html',
            styleUrls: ['components/orderdetail/orderdetail.css'],
            providers: [orderdetail_service_1.orderDetailService]
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            orderdetail_service_1.orderDetailService,
            ngx_electron_1.ElectronService])
    ], orderDetailComponent);
    return orderDetailComponent;
}());
exports.orderDetailComponent = orderDetailComponent;
//# sourceMappingURL=orderdetail.component.js.map