import { Component, Directive, ViewChild, ElementRef, NgZone, ChangeDetectorRef, Input, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { orderDetailService } from '../../services/orderdetail.service';
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib/index'
PouchDB.plugin(PouchFind)
import * as moment from "moment";
import {ElectronService} from 'ngx-electron';

@Component({
  selector: 'order-detail',
  templateUrl: 'components/orderdetail/orderdetail.html',
  styleUrls: ['components/orderdetail/orderdetail.css'],
  providers: [orderDetailService]
})

export class orderDetailComponent{
	private id: any;
	private sub: any;
 	orderToShow: any;

	constructor(private _router: ActivatedRoute,
				private _service: orderDetailService,
        private _electronService: ElectronService){}

	ngOnInit(){
		var self = this;
		self.sub = self._router.params.subscribe(params => {
			self.id = +params['id'];
			console.log("ID FROM PARAMS:-----", self.id)
			self._service.findOrder(self.id).then(function(result){
				result.docs.map(function (row) {
	            	self.orderToShow = row;
      			});
				console.log("ORDER TO SHOW FROM ID FROM PARAMS:-----", self.orderToShow)
        		self._electronService.ipcRenderer.send('Print');
			}).catch(function(err){
				console.log(err);
			});
		});
		// self.findOrder(this.id);
	}
	findOrder(id){
		var self = this;
	}

}
