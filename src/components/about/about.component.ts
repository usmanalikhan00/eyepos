import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';
import { User } from '../../models/model-index';

@Component({
  selector: 'about',
  styles: [],
  templateUrl: 'components/about/about.template.html',
  providers: [AuthenticationService]
})

export class About {

    constructor(private _service:AuthenticationService){}

     ngOnInit(){
     }

     logout() {
         this._service.logout();
     }
}
