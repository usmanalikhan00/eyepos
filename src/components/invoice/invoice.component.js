"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var authentication_service_1 = require("../../services/authentication.service");
var products_service_1 = require("../../services/products.service");
var refundorder_service_1 = require("../../services/refundorder.service");
var moment = require("moment");
var PouchDB = require("pouchdb/dist/pouchdb");
var PouchFind = require("pouchdb-find/lib/index");
PouchDB.plugin(PouchFind);
var Invoice = (function () {
    function Invoice(_service, _authService, _productService, _router) {
        this._service = _service;
        this._authService = _authService;
        this._productService = _productService;
        this._router = _router;
        this.DB = new PouchDB('products');
        this.cloudantDB = new PouchDB('https://aajeel.cloudant.com/products', {
            auth: {
                username: 'aajeel',
                password: 'eyeteaco'
            }
        });
        this.refundOrdersDB = new PouchDB('refundorders');
        this.cloudantRefundOrdersDB = new PouchDB('https://aajeel.cloudant.com/refundorders', {
            auth: {
                username: 'aajeel',
                password: 'eyeteaco'
            }
        });
        this.results = [];
        this.orderToRefund = null;
        this.itemsToRefund = [];
        this.isRefund = true;
        this.isInList = true;
        this.isRefundSave = true;
        this.firstCount = 0;
    }
    Invoice.prototype.ngOnInit = function () {
        var self = this;
        this.scanedBarcode = "";
        var db = self.refundOrdersDB.sync(self.cloudantRefundOrdersDB, {
            live: true,
            retry: true
        });
        db.on('change', function (change) {
            console.log("Info from Change sync:", change);
        }).on('paused', function (info) {
            console.log("Info from Paused sync:", info);
        }).on('active', function () {
            console.log("Info from active sync:");
        }).on('error', function (err) {
            console.log("Info from Error sync:", err);
        });
    };
    Invoice.prototype.logout = function () {
        this._authService.logout();
    };
    Invoice.prototype.process_barcode = function (e) {
        if (e.charCode == 13) {
            this.results.push(this.scanedBarcode);
            this.scanedBarcode = "";
        }
    };
    Invoice.prototype.refundOrder = function (e) {
        var self = this;
        self.orderNumber = e;
        this.isRefund = true;
        this.isInList = true;
        console.log("refundOrder ID: ", self.orderNumber);
        self._service.getRefundOrder(self.orderNumber).then(function (result) {
            console.log(result);
            console.log("temp result docs: ", result.docs.length);
            if (result.docs.length != 0) {
                result.docs.map(function (row) {
                    self.orderItems = row.orderItems;
                    self.orderToRefund = row;
                });
            }
            if (result.docs.length == 0) {
                self.orderToRefund = null;
                self.itemsToRefund = [];
                self.refundAmount = 0;
            }
            self.itemsToRefund = [];
            self.refundAmount = 0;
        }).catch(function (err) {
            console.log(err);
        });
    };
    Invoice.prototype.refundItems = function (product) {
        var self = this;
        var sum = 0;
        var flag = true;
        var refundFlag = true;
        console.log("product to add: ", product);
        self.isRefundSave = true;
        self.isRefund = true;
        self.isInList = true;
        var checkIfRefund = [];
        this._service.checkRefunded(this.orderToRefund._id).then(function (result) {
            console.log("result from refunded orders db: ", result);
            result.docs.map(function (row) {
            });
            console.log("result from check if refunded: ", checkIfRefund);
            for (var i = 0; i < checkIfRefund.length; i++) {
                if (product._id == checkIfRefund[i]._id) {
                    console.log("macthed value, already refund: ", checkIfRefund[i]);
                    refundFlag = false;
                    self.isRefund = false;
                    return 0;
                }
                else {
                    refundFlag = true;
                    self.isRefund = true;
                }
            }
            if ((self.itemsToRefund.length == 0) && (refundFlag)) {
                console.log("macthed value first entry: ", refundFlag);
                self.itemsToRefund.push(product);
                for (var i = 0; i < self.itemsToRefund.length; ++i) {
                    sum = sum + parseInt(self.itemsToRefund[i].cartPrice);
                }
                self.refundAmount = sum;
                self.firstCount++;
            }
            if ((refundFlag) && (self.itemsToRefund.length != 0) && (self.firstCount < 1)) {
                console.log("array length greater than zero..");
                for (var i = 0; i < self.itemsToRefund.length; i++) {
                    if (product._id == self.itemsToRefund[i]._id) {
                        flag = false;
                        self.isInList = false;
                        return 0;
                    }
                    else {
                        flag = true;
                        self.isInList = true;
                    }
                }
                console.log("macthed value after flagself: ", flag);
            }
            if ((flag) && (self.itemsToRefund.length < self.orderItems.length) && (refundFlag) && (self.firstCount < 1)) {
                console.log("macthed value in refund entry: ", flag);
                self.itemsToRefund.push(product);
                for (var i = 0; i < self.itemsToRefund.length; ++i) {
                    sum = sum + parseInt(self.itemsToRefund[i].cartPrice);
                }
                self.refundAmount = sum;
            }
            if (self.firstCount >= 1) {
                self.firstCount = 0;
            }
        }).catch(function (err) {
            console.log("error from refunded orders db: ", err);
        });
    };
    Invoice.prototype.clearRefundItems = function () {
        this.itemsToRefund = [];
        this.refundAmount = 0;
        this.isRefundSave = true;
        this.isRefund = true;
        this.isInList = true;
    };
    Invoice.prototype.saveRefund = function (orderToRefund, saveRefund) {
        var self = this;
        var checkIfRefund = null;
        var productsToUpdate = [];
        var refundDetail = { "_id": new Date().toISOString(),
            "operatorName": localStorage.getItem("userRole"),
            "refundItems": saveRefund,
            "refundAmount": self.refundAmount,
            "refundOrderId": self.orderToRefund.orderId,
            "refundOrderKey": self.orderToRefund._id,
            "refundDay": moment(new Date()).format('DD'),
            "refundMonth": moment(new Date()).format('MMMM'),
            "refundYear": moment(new Date()).format('YYYY')
        };
        for (var i = 0; i < saveRefund.length; i++) {
            self._productService.updateRedundStock(saveRefund[i]._id).then(function (result) {
                console.log("PRODUCT TO UPDATE STOCK VALUE: ", result);
                if (result) {
                    var productDoc = {};
                }
                console.log("PRODUCT DOC TO UPDATE: ", productDoc);
                self._productService.updateRefundStock(productDoc).then(function (result) {
                    console.log("RESULT AFTER UPDATE THE STOCK VALUE: ", result);
                }).catch(function (err) {
                    console.log(err);
                });
            }).catch(function (err) {
                console.log(err);
            });
        }
        self._service.checkRefunded(orderToRefund._id).then(function (result) {
            console.log(result);
            result.docs.map(function (row) {
                checkIfRefund = row;
            });
            console.log("CHECK IF REFUND", checkIfRefund);
            if (checkIfRefund != null) {
                for (var i = 0; i < saveRefund.length; i++) {
                    checkIfRefund.refundItems.push(saveRefund[i]);
                }
                console.log("CHECKIFREFUND DOCUMENT ITEMS AFTER FOR LOOP: ", checkIfRefund.refundItems);
                var updateRefundDetail = { "_id": checkIfRefund._id,
                    "_rev": checkIfRefund._rev,
                    "operatorName": localStorage.getItem("userRole"),
                    "refundItems": checkIfRefund.refundItems,
                    "refundAmount": parseInt(self.refundAmount) + parseInt(checkIfRefund.refundAmount),
                    "refundOrderId": self.orderToRefund.orderId,
                    "refundOrderKey": self.orderToRefund._id,
                    "refundDay": moment(new Date()).format('DD'),
                    "refundMonth": moment(new Date()).format('MMMM'),
                    "refundYear": moment(new Date()).format('YYYY')
                };
                console.log("NEW UPDATE DOCUMENT IF ALREDY REFUNDED: ", updateRefundDetail);
                self._service.updateRefundOrder(updateRefundDetail).then(function (result) {
                    console.log("result from update refund: ", result);
                    self.itemsToRefund = [];
                    self.refundAmount = 0;
                    self.isRefundSave = false;
                }).catch(function (err) {
                    console.log("error from update refund: ", err);
                });
            }
            else {
                console.log("PREVIOUS DOCUMENT IF NOT ALREDY REFUNDED:: ", refundDetail);
                self._service.updateRefundOrder(refundDetail).then(function (result) {
                    console.log("result from save refund: ", result);
                    self.itemsToRefund = [];
                    self.refundAmount = 0;
                    self.isRefundSave = false;
                }).catch(function (err) {
                    console.log("error from save refund: ", err);
                });
            }
        }).catch(function (err) {
            console.log(err);
        });
    };
    Invoice.prototype.removeProduct = function (refundProducts, product, i) {
        var sum = 0;
        refundProducts.splice(i, 1);
        this.itemsToRefund = refundProducts;
        for (var i_1 = 0; i_1 < this.itemsToRefund.length; ++i_1) {
            sum = sum + parseInt(this.itemsToRefund[i_1].cartPrice);
        }
        this.refundAmount = sum;
    };
    Invoice.prototype.goBack = function () {
        console.log("back called");
        this._router.navigate(['pos']);
    };
    __decorate([
        core_1.Input('orderTotal'),
        __metadata("design:type", Object)
    ], Invoice.prototype, "orderTotal", void 0);
    Invoice = __decorate([
        core_1.Component({
            selector: 'invoice',
            styleUrls: ['components/invoice/invoice.css'],
            templateUrl: 'components/invoice/invoice.html',
            providers: [authentication_service_1.AuthenticationService, refundorder_service_1.refundOrderService, products_service_1.ProductsService]
        }),
        __metadata("design:paramtypes", [refundorder_service_1.refundOrderService,
            authentication_service_1.AuthenticationService,
            products_service_1.ProductsService,
            router_1.Router])
    ], Invoice);
    return Invoice;
}());
exports.Invoice = Invoice;
//# sourceMappingURL=invoice.component.js.map