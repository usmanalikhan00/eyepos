import { Component, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';
import {ProductsService} from '../../services/products.service';
import {refundOrderService} from '../../services/refundorder.service';
import { User } from '../../models/model-index';
import * as moment from "moment";
import * as PouchDB  from 'pouchdb/dist/pouchdb';
import * as PouchFind from 'pouchdb-find/lib/index'
PouchDB.plugin(PouchFind)
require('events').EventEmitter.prototype._maxListeners = 100;

@Component({
  selector: 'invoice',
  styleUrls: ['components/invoice/invoice.css'],
  templateUrl: 'components/invoice/invoice.html',
  providers: [AuthenticationService, refundOrderService, ProductsService]
})

export class Invoice {
  @Input('orderTotal') orderTotal;

  public DB = new PouchDB('products');
  // public remoteDB = new PouchDB('http://127.0.0.1:5984/myremotedb');
  public cloudantDB = new PouchDB('https://aajeel.cloudant.com/products', {
    auth: {
      username: 'aajeel',
      password: 'aajeel@eyetea.co'
    }
  });
  public refundOrdersDB = new PouchDB('refundorders');
  public cloudantRefundOrdersDB = new PouchDB('https://aajeel.cloudant.com/refundorders', {
    auth: {
      username: 'aajeel',
      password: 'aajeel@eyetea.co'
    }
  });
  private scanedBarcode;
  private results = [];
  orderNumber: any;
  orderToRefund: any = null;
  itemsToRefund = [];
  orderItems: any;
  refundAmount: any;
  isRefund: boolean = true;
  isInList: boolean  = true;
  isRefundSave: boolean  = true;
  firstCount = 0;

  constructor(private _service: refundOrderService, 
              private _authService: AuthenticationService,
              private _productService: ProductsService,
              private _router: Router){}

  ngOnInit(){
    var self = this;
    this.scanedBarcode = ""
    var db = self.refundOrdersDB.sync(self.cloudantRefundOrdersDB, {
              live: true,
              retry: true
    });
    db.on('change', function (change) {
      // this.updateDocs();
      // this.changes(change);
      console.log("Info from Change sync:", change);
      // self.updateDocs();
      // replication was paused, usually because of a lost connection
    }).on('paused', function (info) {
      console.log("Info from Paused sync:", info);
      // replication was resumed
    }).on('active', function () {
      // yo, something changed!
      console.log("Info from active sync:");
    }).on('error', function (err) {
      console.log("Info from Error sync:", err);
      // totally unhandled error (shouldn't happen)
    });
  }

  logout() {
     this._authService.logout();
  }

  process_barcode(e) {
   if(e.charCode == 13) {
     this.results.push(this.scanedBarcode)
     this.scanedBarcode = ""
   }

  }
  refundOrder(e){
   var self = this;
   self.orderNumber = e;
   this.isRefund = true;
   this.isInList = true;
   console.log("refundOrder ID: ", self.orderNumber);
   self._service.getRefundOrder(self.orderNumber).then(function(result){
     console.log(result);
     console.log("temp result docs: ",result.docs.length);
     if (result.docs.length != 0){
      result.docs.map(function (row) { 
        self.orderItems = row.orderItems;
        self.orderToRefund = row;
      });
     }
     if(result.docs.length == 0){
       self.orderToRefund = null;
       self.itemsToRefund = [];
       self.refundAmount = 0;
     }
     // console.log(self.orderToRefund);
     // console.log(self.itemsToRefund);
     self.itemsToRefund = [];
     self.refundAmount = 0;
   }).catch(function(err){
     console.log(err);
   });
  }
  refundItems(product){
   var self = this;
   var sum = 0;
   var flag = true;
   var refundFlag = true;
   console.log("product to add: ",product);
   // console.log("refund itenms length from order array: ", this.orderItems.length);
   // console.log("refund items list length: ", this.itemsToRefund.length);
     // this.itemsToRefund.push(product);
   // console.log("itenms to refund: ",this.itemsToRefund);
   self.isRefundSave = true;
   self.isRefund = true;
   self.isInList = true;
   var checkIfRefund = [];
   this._service.checkRefunded(this.orderToRefund._id).then(function(result){
     console.log("result from refunded orders db: ", result);
      result.docs.map(function (row) { 
        checkIfRefund = row.refundItems;
      });
     console.log("result from check if refunded: ", checkIfRefund);
     for(let i=0; i<checkIfRefund.length; i++){
       if (product._id == checkIfRefund[i]._id){
         console.log("macthed value, already refund: ", checkIfRefund[i]);
         refundFlag = false;
         self.isRefund = false;
         return 0;
       }else{
         refundFlag = true;
         self.isRefund = true;
       }
     }
     if((self.itemsToRefund.length == 0) && (refundFlag)){
       console.log("macthed value first entry: ", refundFlag);
       self.itemsToRefund.push(product);
       for (let i = 0; i < self.itemsToRefund.length; ++i) {
         sum = sum + parseInt(self.itemsToRefund[i].cartPrice);
       }     
       self.refundAmount = sum;
       self.firstCount++;
     } 
     if ((refundFlag) && (self.itemsToRefund.length != 0) && (self.firstCount < 1)){
       console.log("array length greater than zero..");
       for(let i=0; i<self.itemsToRefund.length; i++){
         if(product._id == self.itemsToRefund[i]._id){
           flag = false;
           self.isInList = false;
           return 0;
         }else{
           flag = true;
           self.isInList = true;
         }
       }
       console.log("macthed value after flagself: ", flag);
     }
     if((flag) && (self.itemsToRefund.length < self.orderItems.length) && (refundFlag) && (self.firstCount < 1)){
       console.log("macthed value in refund entry: ", flag);
       self.itemsToRefund.push(product);
       for (let i = 0; i < self.itemsToRefund.length; ++i) {
         sum = sum + parseInt(self.itemsToRefund[i].cartPrice);
       }
       self.refundAmount = sum;
     }
     if(self.firstCount >= 1){
       self.firstCount = 0;
     }
   }).catch(function(err){
     console.log("error from refunded orders db: ", err);
   });
  }
  clearRefundItems(){
    this.itemsToRefund = [];
    this.refundAmount = 0;
    this.isRefundSave = true;
    this.isRefund = true;
    this.isInList = true;
  }
  saveRefund(orderToRefund, saveRefund){
    var self = this;
    var checkIfRefund: any = null;
    var productsToUpdate: any = [];
    var refundDetail = { "_id": new Date().toISOString(),  
                         "operatorName": localStorage.getItem("userRole"),
                         "refundItems": saveRefund,
                         "refundAmount": self.refundAmount,
                         "refundOrderId": self.orderToRefund.orderId,
                         "refundOrderKey": self.orderToRefund._id,
                         "refundDay": moment(new Date()).format('DD'),
                         "refundMonth": moment(new Date()).format('MMMM'),
                         "refundYear": moment(new Date()).format('YYYY')
                        }
    for (let i = 0; i < saveRefund.length; i++){
      self._productService.updateRedundStock(saveRefund[i]._id).then(function(result){
        console.log("PRODUCT TO UPDATE STOCK VALUE: ", result);
        var productDoc: any;
        if (result){
          if (result.type === 'singleunit'){

            productDoc = {"_id": result._id,
                          "_rev": result._rev,
                          "name": result.name,
                          "price": result.price,
                          "type": result.type,
                          "weight": result.weight,
                          "productBarCode": result.productBarCode,
                          "stockval": result.stockval + saveRefund[i].quantity
                          }

          }else{
            productDoc = {"_id": result._id,
                          "_rev": result._rev,
                          "name": result.name,
                          "price": result.price,
                          "type": result.type,
                          "stockval": result.stockval,
                          "productBarCode": result.productBarCode,
                          "weight": result.weight + saveRefund[i].cartStockWeight
                          }
          }
          // var productDoc = {}
        }
        console.log("PRODUCT DOC TO UPDATE: ", productDoc);
        self._productService.updateRefundStock(productDoc).then(function(result){
          console.log("RESULT AFTER UPDATE THE STOCK VALUE: ", result);
          // var db = self.DB.sync(self.cloudantDB, {
          //     live: true,
          //     retry: true
          // });
          // db.on('change', function (change) {
          //   // this.updateDocs();
          //   // this.changes(change);
          //   console.log("Info from Change sync:", change);
          //   // self.updateDocs();
          //   // replication was paused, usually because of a lost connection
          // }).on('paused', function (info) {
          //   console.log("Info from Paused sync:", info);
          //   // replication was resumed
          // }).on('active', function () {
          //   // yo, something changed!
          //   console.log("Info from active sync:");
          // }).on('error', function (err) {
          //   console.log("Info from Error sync:", err);
          //   // totally unhandled error (shouldn't happen)
          // });
          self._service.checkRefunded(orderToRefund._id).then(function(result){
            console.log(result);
            result.docs.map(function (row) { 
              checkIfRefund = row;
            });
            console.log("CHECK IF REFUND", checkIfRefund);
            if(checkIfRefund != null){
              for(let i =0; i<saveRefund.length; i++){
                checkIfRefund.refundItems.push(saveRefund[i]);
              }
              console.log("CHECKIFREFUND DOCUMENT ITEMS AFTER FOR LOOP: ", checkIfRefund.refundItems);
              var updateRefundDetail = { "_id": checkIfRefund._id,
                                         "_rev": checkIfRefund._rev,  
                               "operatorName": localStorage.getItem("userRole"),
                               "refundItems": checkIfRefund.refundItems,
                               "refundAmount": parseInt(self.refundAmount) + parseInt(checkIfRefund.refundAmount),
                               "refundOrderId": self.orderToRefund.orderId,
                               "refundOrderKey": self.orderToRefund._id,
                               "refundDay": moment(new Date()).format('DD'),
                               "refundMonth": moment(new Date()).format('MMMM'),
                               "refundYear": moment(new Date()).format('YYYY')
                              }  
              console.log("NEW UPDATE DOCUMENT IF ALREDY REFUNDED: ", updateRefundDetail);
              self._service.updateRefundOrder(updateRefundDetail).then(function(result){
                 console.log("result from update refund: ", result);
                 self.itemsToRefund = [];
                 self.refundAmount = 0;
                 self.isRefundSave = false;
                 // var self = this;

                  // var db = self.refundOrdersDB.sync(self.cloudantRefundOrdersDB, {
                  //   live: true,
                  //   retry: true
                  // });
                  // db.on('change', function (change) {
                  //   // this.updateDocs();
                  //   // this.changes(change);
                  //   console.log("Info from Change sync:", change);
                  //   // self.updateDocs();
                  //   // replication was paused, usually because of a lost connection
                  // }).on('paused', function (info) {
                  //   console.log("Info from Paused sync:", info);
                  //   // replication was resumed
                  // }).on('active', function (info) {
                  //   // yo, something changed!
                  //   console.log("Info from active sync:", info);
                  // }).on('error', function (err) {
                  //   console.log("Info from Error sync:", err);
                  //   // totally unhandled error (shouldn't happen)
                  // });
              }).catch(function(err){
                console.log("error from update refund: ", err)
              });
            }else{
              console.log("PREVIOUS DOCUMENT IF NOT ALREDY REFUNDED:: ", refundDetail);
              self._service.updateRefundOrder(refundDetail).then(function(result){
                 console.log("result from save refund: ", result);
                 self.itemsToRefund = [];
                 self.refundAmount = 0;
                 self.isRefundSave = false;           
              }).catch(function(err){
                console.log("error from save refund: ", err)
              });
            }
          }).catch(function(err){
            console.log(err);
          });
        }).catch(function(err){
          console.log(err);
        });
      }).catch(function(err){
        console.log(err);
      });
    }

  }
  removeProduct(refundProducts, product, i){
    var sum = 0;
    refundProducts.splice(i, 1);
    this.itemsToRefund = refundProducts;
    for (let i = 0; i < this.itemsToRefund.length; ++i) {
      sum = sum + parseInt(this.itemsToRefund[i].cartPrice);
    }
    this.refundAmount = sum;
  }
  goBack(){
    console.log("back called");
    this._router.navigate(['pos']);
  }
}
