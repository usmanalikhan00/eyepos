import { Component, ViewChild, ElementRef, NgZone, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';
import {ProductsService} from '../../services/products.service';
import {PosService} from '../../services/pos.service';
import { User, Product } from '../../models/model-index';
import { Location } from '@angular/common';
import {Headers, Http} from '@angular/http';
import {HostListener} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import * as PouchDB  from 'pouchdb/dist/pouchdb';

@Component({
  selector: 'product-form',
  templateUrl: 'components/product/product.html',
  styleUrls: ['components/product/product.css'],
  providers: [AuthenticationService, ProductsService, PosService]
  // host: {
  //       '(document:keypress)': 'handleKeyboardEvents($event)'
  //   }
})

export class productComponent{
  
  @ViewChild('closeBtn') closeBtn: ElementRef;
  @ViewChild('closeEditModal') closeEditModal: ElementRef;
  
  addProductForm: FormGroup;
  editProductForm: FormGroup;
  allProducts: any = [];
  docId: any;
  scannedExists: boolean = true;
  totalStockValue: any = 0;
  availProducts: any;
  unAvailProducts: any;
  totalProducts: any;

  public barCode: any = '';
  public data: any;
  public filterQuery: any = "";
  public rowsOnPage: any = 10;
  public sortBy: any = "name";
  public sortOrder: any = "asc";
  public key: any;

  public DB = new PouchDB('products');
  // public remoteDB = new PouchDB('http://127.0.0.1:5984/myremotedb');
  public cloudantDB = new PouchDB('https://aajeel.cloudant.com/products', {
    auth: {
      username: 'aajeel',
      password: 'aajeel@eyetea.co'
    }
  });


  public cloudantProductsDB = new PouchDB('https://360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com/aajeelproducts', {
    auth: {
      "username": "360527dd-869f-46ae-be21-c9b27c672b55-bluemix",
      "password": "591b1db46a44e67d7aeaf6323b36ee12b4484d03942c2f5dc942489d99417e9e",
      "host": "360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com",
      "port": 443,
      "url": "https://360527dd-869f-46ae-be21-c9b27c672b55-bluemix:591b1db46a44e67d7aeaf6323b36ee12b4484d03942c2f5dc942489d99417e9e@360527dd-869f-46ae-be21-c9b27c672b55-bluemix.cloudant.com"
    }
    // auth: {
    //   username: 'arsie108@gmail.com',
    //   password: 'Admin123@'
    // }
  });

  typePlaceholder: any = "Enter Price Per Unit"
  editPlaceholder: any = "Enter Price Per Unit"

  constructor(private _ref: ChangeDetectorRef, 
              private _service: ProductsService, 
              private _posService: PosService, 
              private _location: Location, 
              private _formBuilder: FormBuilder, 
              public _zone: NgZone, 
              public _http: Http ) {
    this._buildAddProductForm();
    this._buildEditProductForm();
  }
  private closeModal() {
    this.closeBtn.nativeElement.click();
  }
  private _buildAddProductForm() {
    this.addProductForm = this._formBuilder.group({
      name: ["", Validators.compose([Validators.required, Validators.minLength(3)])],
      type: ["", Validators.compose([Validators.required])],
      weight: ["", Validators.required],
      price: ["", Validators.required],
      stockval: ["", Validators.required],
      productBarCode: ["", Validators.required]
    });
  }  
  private _buildEditProductForm() {
    this.editProductForm = this._formBuilder.group({
      name: ["", Validators.compose([Validators.required, Validators.minLength(3)])],
      price: ["", Validators.compose([Validators.required])],
      type: ['', Validators.compose([Validators.required])],
      weight: ["", Validators.compose([Validators.required])],
      stockval: ["", Validators.compose([Validators.required])],
      productBarCode: ["", Validators.required]
    });
  }

  // handleKeyboardEvents(event: KeyboardEvent) { 
  //   this.key = event.key;
  //   // this.key = event.which || event.keyCode;
  //   // console.log(this.key);
    
  //   if (event.charCode != 13)
  //     this.barCode = this.barCode+this.key;
  //   else{
  //     console.log(this.barCode);
  //     this.barCode = '';
  //   }
  // }
  syncDb(){
    var self = this
    var opts = { live: true, retry: true };
    self.DB.replicate.from(self.cloudantProductsDB).on('complete', function(info) {
      console.log("COMPLETE EVENT FROM CLOUDANT ONE-WAY PRODUCT REPLICATION:--", info)
      self.DB.sync(self.cloudantProductsDB, opts).on('change', function (info) {
        console.log("CHANGE EVENT FROM TWO-WAY PRODUCTS SYNC:--", info)
        if (info.direction === 'pull' || info.direction === 'push'){
          self.updateDocs()
        }
      }).on('paused', function (err) {
        console.log("PAUSE EVENT FROM TWO-WAY SYNC PRODUCTS", err)
      }).on('active', function () {
        console.log("ACTIVE ACTIVE FROM TWO-WAY PRODUCTS SYNC!!")
      }).on('denied', function (err) {
        console.log("DENIED DENIED !!", err)
      }).on('complete', function (info) {
        console.log("COMPLETED !!", info)
      }).on('error', function (err) {
        console.log("ERROR ERROR FROM SYNC PRODUCTS!!", err)
      })
    }).on('error', function (err) {
      console.log("ERROR ERROR FROM REPLICATION CLOUDANT PRODUCTS !!", err)
    })    
  }
  ngOnInit(){
    var self = this;
    self.syncDb()
    self.updateDocs();
    // var db = self.DB.sync(self.cloudantDB, {
    //   live: true,
    //   retry: true
    // }).on('change', function (change) {
    //   console.log("Info from Change sync:", change);
    //   self.updateDocs();
    // }).on('paused', function (info) {
    //   console.log("Info from Paused sync:", info);
    // }).on('active', function () {
    //   console.log("Info from active sync:");
    // }).on('error', function (err) {
    //   console.log("Info from Error sync:", err);
    // });
  }
  productsCount(allProducts){
    var availProducts = 0;
    var unAvailProducts = 0;
    var totalProducts = 0;
    var sum = 0
    var weightsum = 0
    this.unAvailProducts = 0;
    this.availProducts = 0;

    this.totalProducts = allProducts.length;
    console.log("total Products length: ", this.totalProducts);

    for(let i=0; i<allProducts.length; i++){
      if(allProducts[i].stockval <= 0){
        unAvailProducts = unAvailProducts + 1;
      }else{
        availProducts = availProducts + 1;
      }
      if (allProducts[i].stockval === "none"){
        weightsum = weightsum + (allProducts[i].price * allProducts[i].weight/1000)
      }else{
        sum = sum + (allProducts[i].price * allProducts[i].stockval)
      }
    }
    this.totalStockValue = sum + weightsum
    this.availProducts = availProducts;
    this.unAvailProducts = unAvailProducts;
    console.log("Unavailable Products Count: ", this.unAvailProducts);
    console.log("Available Products Count: ", this.availProducts);
  }
  addProduct(value){
    var self = this
     this.scannedExist()
    if(self.scannedExists){
      var product = new Product(null,'','','','','');
      value.price = parseInt(value.price)
      if (value.type === 'weighted')
        value.weight = parseFloat(value.weight) * 1000
      else
        value.stockval = parseInt(value.stockval)
      product = value;
      console.log("add product values:", product);
      self._service.saveProduct(product).then(function(result){
        self._buildAddProductForm();
        self.updateDocs();
      }).catch(function(err) {
        // body...
        console.log(err)
      });
    }
  }
  scannedExist(){
    console.log(this.addProductForm.controls['productBarCode'].value, "sacnned Bar Code");
    for (let i=0; i<this.allProducts.length; i++){
      if (this.addProductForm.controls['productBarCode'].value === this.allProducts[i].productBarCode){
        console.log("matched at index", this.allProducts[i].productBarCode, i);
        this.scannedExists = false;
        console.log("matched at index scannedExists", this.scannedExists, i);
        (<FormGroup>this.addProductForm).patchValue({'productBarCode':''},{onlySelf: true})
        return 0;
      }else{
        this.scannedExists = true;
        console.log("not matched at index scannedExists", this.scannedExists, i);
      }
    }
    console.log("matched at index scannedExists", this.scannedExists);
  }
  process_barcode(e){
    console.log("value from barcode field", this.addProductForm.controls['productBarCode'].value);
    this.scannedExist();
  }
  editProduct(values){
    var self = this;
    console.log("VALUES FROM EDIT:---", values)
    if (values.type === 'weighted'){
      self.editPlaceholder = "Enter Price Per Kg."
    }else{
      self.editPlaceholder = "Enter Price Per Unit"
    }
    var newValue = {'name': values.name,
                'price': values.price,
                'stockval': values.stockval,
                'type': values.type,
                'weight': values.weight/1000,
                'productBarCode': values.productBarCode};
    (<FormGroup>this.editProductForm).setValue(newValue, { onlySelf: true });
  }
  saveProduct(newValues, product){
    var self = this;
    console.log("In save product ID: ", product);
    newValues._id = product._id
    newValues._rev = product._rev
    if (newValues.type === 'weighted'){
      newValues.weight = parseFloat(newValues.weight)*1000
    }
    console.log("New Values in save product: ", newValues);
    this._service.editProduct(newValues).then(function(result){
      console.log(result);
      self.allProducts = [];
      self._service.getAllProducts().then(function (result) {
          result.rows.map(function (row) { 
            self.allProducts.push(row.doc); 
          });
          self.productsCount(self.allProducts);
          console.log("Call From ngOnInit(): ", self.allProducts);
      }).catch(function (err) {
        console.log(err);
      });
    }).catch(function (err){
      console.log(err);
    });
  }
  deleteProduct(doc) {
    var self = this;
    // self.allProducts = [];
    this._service.deleteProduct(doc).then(function (doc) {
      return self.DB.remove(doc._id, doc._rev).then(function (doc) {
          self.deleteDocs(doc);
      }).catch(function (err){
        console.log(err);
      });
    }).catch(function (err) {
      console.log(err);
    }); 
    console.log("doc to delete ID: ", doc);
    console.log("all docs after delete: ", self.allProducts);
  }
  typeChanges($event){
    console.log("TYPE CHANGES:)))))", $event.srcElement.value)
    if ($event.srcElement.value === 'singleunit'){
      (<FormGroup>this.addProductForm).patchValue({'weight':'none', 'stockval': '', 'price':''}, {onlySelf: true})
      this.typePlaceholder = "Enter Price Per Unit"
    }else{
      (<FormGroup>this.addProductForm).patchValue({'stockval':'none', 'weight': '', 'price':''}, {onlySelf: true})
      this.typePlaceholder = "Enter Price Per Kg."
    }
  }
  updateDocs(){
    var self = this;
    self.allProducts = [];
    self.totalStockValue = 0;
    self._service.getAllProducts().then(function (result) {
      result.rows.map(function (row) { 
        self.allProducts.push(row.doc);
      });
      self.productsCount(self.allProducts);
      // self._ref.detectChanges();
    }).catch(function (err) {
      console.log(err);
    });
  
  }
  deleteDocs(doc){
    var self = this;
    self.allProducts = [];
    self._service.getAllProducts().then(function (result) {
      result.rows.map(function (row) {
        if (row.doc._id != doc) 
          self.allProducts.push(row.doc); 
      });
      console.log("Call From deleteDocs(): ", self.allProducts);
    }).catch(function (err) {
      console.log(err);
    });
    self._ref.detectChanges();
  }
  goBack(){
    this._location.back();
    this._ref.detach();
  }
  formPriceValidator(val : FormControl) :any {
    console.log('formValidator ' + val.value);
    if(val.value.match(/.*[^0-9].*/)){
      console.log('match');
      return {inval : true};
    }  
    // return {inval : false};  
  }
  editformPriceValidator(val) :any {
    console.log('formValidator ' + val.value);
    // if(val.value.match(/.*[^0-9].*/)){
    //   console.log('match');
    //   return {inval : true};
    // }  
    // return {inval : false};  
  }

  formStockValidator(val : FormControl) :any {
    console.log('formValidator ' + val.value);
    if(val.value.match(/.*[^0-9].*/)){
      console.log('match');
      return {inval : true};
    }
      // return {inval : false};

  }  
  editformStockValidator(val) :any {
    console.log('formValidator ' + val.value);
    // if(val.value.match(/.*[^0-9].*/)){
    //   console.log('match');
    //   return {inval : true};
    // }
      // return {inval : false};

  }

}