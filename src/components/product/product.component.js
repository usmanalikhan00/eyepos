"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var authentication_service_1 = require("../../services/authentication.service");
var products_service_1 = require("../../services/products.service");
var model_index_1 = require("../../models/model-index");
var common_1 = require("@angular/common");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var forms_1 = require("@angular/forms");
var PouchDB = require("pouchdb/dist/pouchdb");
var productComponent = (function () {
    function productComponent(_ref, _service, _location, _formBuilder, _zone, _http) {
        this._ref = _ref;
        this._service = _service;
        this._location = _location;
        this._formBuilder = _formBuilder;
        this._zone = _zone;
        this._http = _http;
        this.allProducts = [];
        this.scannedExists = true;
        this.totalStockValue = 0;
        this.barCode = '';
        this.filterQuery = "";
        this.rowsOnPage = 10;
        this.sortBy = "name";
        this.sortOrder = "asc";
        this.DB = new PouchDB('products');
        this.cloudantDB = new PouchDB('https://aajeel.cloudant.com/products', {
            auth: {
                username: 'aajeel',
                password: 'eyeteaco'
            }
        });
        this._buildAddProductForm();
        this._buildEditProductForm();
    }
    productComponent.prototype.closeModal = function () {
        this.closeBtn.nativeElement.click();
    };
    productComponent.prototype._buildAddProductForm = function () {
        this.addProductForm = this._formBuilder.group({
            name: ["", forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(3)])],
            price: ["", forms_1.Validators.compose([forms_1.Validators.required, this.formPriceValidator])],
            stockval: ["", forms_1.Validators.compose([forms_1.Validators.required, this.formStockValidator])],
            productBarCode: ["", forms_1.Validators.required]
        });
    };
    productComponent.prototype._buildEditProductForm = function () {
        this.editProductForm = this._formBuilder.group({
            name: ["", forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(3)])],
            price: ["", forms_1.Validators.compose([forms_1.Validators.required])],
            stockval: ["", forms_1.Validators.compose([forms_1.Validators.required])],
            productBarCode: ["", forms_1.Validators.required]
        });
    };
    productComponent.prototype.syncDb = function () {
    };
    productComponent.prototype.ngOnInit = function () {
        var self = this;
        var db = self.DB.sync(self.cloudantDB, {
            live: true,
            retry: true
        }).on('change', function (change) {
            console.log("Info from Change sync:", change);
            self.updateDocs();
        }).on('paused', function (info) {
            console.log("Info from Paused sync:", info);
        }).on('active', function () {
            console.log("Info from active sync:");
        }).on('error', function (err) {
            console.log("Info from Error sync:", err);
        });
        self.updateDocs();
    };
    productComponent.prototype.productsCount = function (allProducts) {
        var availProducts = 0;
        var unAvailProducts = 0;
        var totalProducts = 0;
        var sum = 0;
        this.unAvailProducts = 0;
        this.availProducts = 0;
        this.totalProducts = allProducts.length;
        console.log("total Products length: ", this.totalProducts);
        for (var i = 0; i < allProducts.length; i++) {
            if (allProducts[i].stockval == 0) {
                unAvailProducts = unAvailProducts + 1;
            }
            else {
                availProducts = availProducts + 1;
            }
            sum = sum + (allProducts[i].price * allProducts[i].stockval);
        }
        this.totalStockValue = (sum);
        this.availProducts = availProducts;
        this.unAvailProducts = unAvailProducts;
        console.log("Unavailable Products Count: ", this.unAvailProducts);
        console.log("Available Products Count: ", this.availProducts);
    };
    productComponent.prototype.addProduct = function (value) {
        if (this.scannedExists) {
            var product = new model_index_1.Product(null, '', '', '', '');
            product = value;
            console.log("add product values:", value);
            this._service.saveProduct(product).then(function (result) {
                this._buildAddProductForm();
                this.updateDocs();
            }).catch(function (err) {
                console.log(err);
            });
        }
    };
    productComponent.prototype.scannedExist = function () {
        console.log(this.addProductForm.controls['productBarCode'].value, "sacnned Bar Code");
        for (var i = 0; i < this.allProducts.length; i++) {
            if (this.addProductForm.controls['productBarCode'].value === this.allProducts[i].productBarCode) {
                console.log("matched at index", this.allProducts[i].productBarCode, i);
                this.scannedExists = false;
                console.log("matched at index scannedExists", this.scannedExists, i);
                return false;
            }
            else {
                this.scannedExists = true;
                console.log("matched at index scannedExists", this.scannedExists, i);
            }
        }
        console.log("matched at index scannedExists", this.scannedExists);
    };
    productComponent.prototype.process_barcode = function (e) {
        if (e.charCode == 13) {
            this.scannedExist();
            return false;
        }
    };
    productComponent.prototype.editProduct = function (values) {
        var self = this;
        var newValues = { 'name': values.name,
            'price': values.price,
            'stockval': values.stockval,
            'productBarCode': values.productBarCode };
        this.editProductForm.setValue(newValues, { onlySelf: true });
    };
    productComponent.prototype.saveProduct = function (newValues, product) {
        var self = this;
        var jQuery;
        console.log("In save product ID: ", product);
        console.log("New Values in save product: ", newValues);
        this._service.editProduct(newValues, product).then(function (result) {
            console.log(result);
            self.allProducts = [];
            self._service.getAllProducts().then(function (result) {
                result.rows.map(function (row) {
                    self.allProducts.push(row.doc);
                });
                self.productsCount(self.allProducts);
                console.log("Call From ngOnInit(): ", self.allProducts);
            }).catch(function (err) {
                console.log(err);
            });
        }).catch(function (err) {
            console.log(err);
        });
    };
    productComponent.prototype.deleteProduct = function (doc) {
        var self = this;
        this._service.deleteProduct(doc).then(function (doc) {
            return self.DB.remove(doc._id, doc._rev).then(function (doc) {
                self.deleteDocs(doc);
            }).catch(function (err) {
                console.log(err);
            });
        }).catch(function (err) {
            console.log(err);
        });
        console.log("doc to delete ID: ", doc);
        console.log("all docs after delete: ", self.allProducts);
    };
    productComponent.prototype.updateDocs = function () {
        var self = this;
        self.allProducts = [];
        self.totalStockValue = 0;
        self._service.getAllProducts().then(function (result) {
            result.rows.map(function (row) {
                self.allProducts.push(row.doc);
            });
            self.productsCount(self.allProducts);
            console.log("Call From ngOnInit(): ", self.allProducts);
        }).catch(function (err) {
            console.log(err);
        });
        self._ref.detectChanges();
    };
    productComponent.prototype.deleteDocs = function (doc) {
        var self = this;
        self.allProducts = [];
        self._service.getAllProducts().then(function (result) {
            result.rows.map(function (row) {
                if (row.doc._id != doc)
                    self.allProducts.push(row.doc);
            });
            console.log("Call From deleteDocs(): ", self.allProducts);
        }).catch(function (err) {
            console.log(err);
        });
        self._ref.detectChanges();
    };
    productComponent.prototype.goBack = function () {
        this._location.back();
        this._ref.detach();
    };
    productComponent.prototype.formPriceValidator = function (val) {
        console.log('formValidator ' + val.value);
        if (val.value.match(/.*[^0-9].*/)) {
            console.log('match');
            return { inval: true };
        }
    };
    productComponent.prototype.editformPriceValidator = function (val) {
        console.log('formValidator ' + val.value);
    };
    productComponent.prototype.formStockValidator = function (val) {
        console.log('formValidator ' + val.value);
        if (val.value.match(/.*[^0-9].*/)) {
            console.log('match');
            return { inval: true };
        }
    };
    productComponent.prototype.editformStockValidator = function (val) {
        console.log('formValidator ' + val.value);
    };
    __decorate([
        core_1.ViewChild('closeBtn'),
        __metadata("design:type", core_1.ElementRef)
    ], productComponent.prototype, "closeBtn", void 0);
    __decorate([
        core_1.ViewChild('closeEditModal'),
        __metadata("design:type", core_1.ElementRef)
    ], productComponent.prototype, "closeEditModal", void 0);
    productComponent = __decorate([
        core_1.Component({
            selector: 'product-form',
            templateUrl: 'components/product/product.html',
            styleUrls: ['components/product/product.css'],
            providers: [authentication_service_1.AuthenticationService, products_service_1.ProductsService]
        }),
        __metadata("design:paramtypes", [core_1.ChangeDetectorRef,
            products_service_1.ProductsService,
            common_1.Location,
            forms_1.FormBuilder,
            core_1.NgZone,
            http_1.Http])
    ], productComponent);
    return productComponent;
}());
exports.productComponent = productComponent;
//# sourceMappingURL=product.component.js.map