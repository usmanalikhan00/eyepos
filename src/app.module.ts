import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent }  from './app.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { ROUTES } from './app.routes';
import { AuthGuard } from './_gaurds/auth.gaurd';
import { AuthenticationService } from './services/authentication.service';
import { About, 
         NoContent, 
         processOrder, 
         Login, 
         Invoice, 
         Home,
         productComponent, 
         DataFilterPipe, 
         posComponent, 
         ordersComponent, 
         ordersFilterPipe, 
         refundorders, 
         orderDetailComponent } from './components/components-index';
import { DataTableModule } from "angular2-datatable";
import { HttpModule }    from '@angular/http';
import { ModalModule } from "ng2-modal";
import { AlertModule, ProgressbarModule, TabsModule } from 'ngx-bootstrap';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ToastyModule } from 'ng2-toasty';
import { NgxElectronModule } from 'ngx-electron';
import { ChartsModule } from 'ng2-charts';


// const appRoutes: Routes = [
//   { path: 'crisis-center', component: CrisisListComponent },
//   { path: 'heroes', component: HeroListComponent },
// ];

@NgModule({
  imports: [BrowserModule,
            ToastyModule.forRoot(),
            RouterModule.forRoot(ROUTES, { useHash: true }),
            FormsModule,
            ReactiveFormsModule,
            DataTableModule,
            ModalModule,
            HttpModule,
            ChartsModule,
            NgxElectronModule,
            BsDatepickerModule.forRoot(),
            ProgressbarModule.forRoot(),
            TabsModule.forRoot(),
            AlertModule.forRoot()],
  exports: [BrowserModule, 
            AlertModule, 
            processOrder,
            RouterModule],
  declarations: [ AppComponent, 
                  About, 
                  NoContent, 
                  processOrder, 
                  Login, 
                  Invoice, 
                  Home, 
                  productComponent, 
                  DataFilterPipe, 
                  posComponent, 
                  refundorders, 
                  ordersComponent, 
                  ordersFilterPipe, 
                  orderDetailComponent ],
  providers:    [ AuthGuard,AuthenticationService ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
