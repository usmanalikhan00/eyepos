"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var user_1 = require("./user");
exports.User = user_1.User;
var product_1 = require("./product");
exports.Product = product_1.Product;
var customer_1 = require("./customer");
exports.Customer = customer_1.Customer;
//# sourceMappingURL=model-index.js.map