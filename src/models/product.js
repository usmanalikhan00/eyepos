"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Product = (function () {
    function Product(id, name, price, stockval, productBarCode) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.stockval = stockval;
        this.productBarCode = productBarCode;
    }
    return Product;
}());
exports.Product = Product;
//# sourceMappingURL=product.js.map