"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Customer = (function () {
    function Customer(_id, name, phone, email, address) {
        this._id = _id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.address = address;
    }
    return Customer;
}());
exports.Customer = Customer;
//# sourceMappingURL=customer.js.map