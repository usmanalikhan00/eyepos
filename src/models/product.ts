export class Product {
  constructor(
    public id: number,
    public name: string,
    public price: string,
    public weight: string,
    public stockval: string, 
    public productBarCode: string){ }
}
